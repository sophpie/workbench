<?php
declare(strict_types = 1);
/**
 * Created by PhpStorm.
 * User: sophie.beaupuis
 * Date: 22/12/2016
 * Time: 10:33
 */

namespace Sophpie\Workbench\Probe;


use Sophpie\Workbench\Test\TestEvent;
use Sophpie\Workbench\Test\TestManager;

/**
 * Class ExecutionTimeProbe
 * @package Sophpie\Workbench\Probe
 */
class ExecutionTimeProbe implements ProbeInterface
{
    /**
     * @var float
     */
    protected $executionTime = 0;

    /**
     * @param TestEvent $event
     */
    public function onTestStart(TestEvent $event)
    {
        $this->executionTime = microtime(true);
    }

    public function onTestStop(TestEvent $event)
    {
        die('arg');
        $dif = microtime(true) - $this->executionTime;
        $event->getResult()->addProbeResult(get_class($this),['executionTime' => $dif]);
    }

    /**
     * @inheritDoc
     */
    public static function getSubscribedEvents()
    {
        return [
            TestManager::EVENT_RUN => [
                ['onTestStart', 1],
                ['onTestStop',-1]
            ]
        ];
    }
}