<?php
/**
 * Created by PhpStorm.
 * User: sophie.beaupuis
 * Date: 19/12/2016
 * Time: 14:56
 */

namespace Sophpie\Workbench\Probe;


use Symfony\Component\EventDispatcher\EventSubscriberInterface;

interface ProbeInterface extends EventSubscriberInterface
{
}