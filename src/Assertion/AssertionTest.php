<?php
declare(strict_types = 1);
/**
 * Created by PhpStorm.
 * User: sophie.beaupuis
 * Date: 21/12/2016
 * Time: 10:41
 */

namespace Sophpie\Workbench\Assertion;


use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Reference;

class AssertionTest
{
    /**
     * @var string
     */
    protected $realValueDataPath;

    /**
     * @var mixed
     */
    protected $expectedValue;

    /**
     * @var Definition|Reference
     */
    protected $assertion;

    /**
     * AssertionTest constructor.
     * @param mixed $assertion
     * @param string $realValueDataPath
     * @param mixed $expectedValue
     */
    public function __construct($assertion,string $realValueDataPath, $expectedValue)
    {
        $this->realValueDataPath = $realValueDataPath;
        $this->expectedValue = $expectedValue;
        $this->assertion = $assertion;
    }

    /**
     * @return string
     */
    public function getRealValueDataPath(): string
    {
        return $this->realValueDataPath;
    }

    /**
     * @return mixed
     */
    public function getExpectedValue()
    {
        return $this->expectedValue;
    }

    /**
     * @return Definition|Reference
     */
    public function getAssertion()
    {
        return $this->assertion;
    }

    /**
     * @param mixed $expectedValue
     */
    public function setExpectedValue($expectedValue)
    {
        $this->expectedValue = $expectedValue;
    }

    /**
     * @param Definition|Reference $assertion
     */
    public function setAssertion($assertion)
    {
        $this->assertion = $assertion;
    }
}