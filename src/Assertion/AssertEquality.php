<?php
declare(strict_types = 1);
/**
 * Created by PhpStorm.
 * User: sophie.beaupuis
 * Date: 19/12/2016
 * Time: 15:10
 */

namespace Sophpie\Workbench\Assertion;


/**
 * Class AssertEquality
 * @package Sophpie\Workbench\Assertion
 */
class AssertEquality
{
    /**
     * @inheritDoc
     */
    public function isValid($realValue,$expected): bool
    {
        //@todo : adding comment into assertion result
        return $realValue == $expected;
    }

}