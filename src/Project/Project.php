<?php
declare(strict_types = 1);
/**
 * Created by PhpStorm.
 * User: sophie.beaupuis
 * Date: 21/12/2016
 * Time: 14:39
 */

namespace Sophpie\Workbench\Project;


use Sophpie\Workbench\DependencyInjection\ContainerNode;
use Sophpie\Workbench\Test\Outline\OutlineAbstract;
use Sophpie\Workbench\Test\Context\ContextOutline;
use Sophpie\Workbench\Test\Outline\SampleOutlineProvider;
use Sophpie\Workbench\Test\Sample\SampleOutline;

/**
 * Class Project
 * @package Sophpie\Workbench\Project
 */
class Project extends OutlineAbstract implements SampleOutlineProvider
{
    /**
     * Source code path
     * @var string
     */
    protected $workingDirectory = '';

    /**
     * @var string
     */
    protected $name;

    /**
     * @var array
     */
    protected $probes = [];

    /**
     * @var array
     */
    protected $contextOutlines = [];

    /**
     * @inheritDoc
     */
    function configureFromArray(array $data)
    {
        $this->setName($data['name']);
        if (isset($data['workingDirectory'])) {
            $this->setWorkingDirectory($data['workingDirectory']);
        }
        if (isset($data['probes'])) {
            foreach ($data['probes'] as $containerNodeData) {
                $probeNode = ContainerNode::factory($containerNodeData);
                $this->probes[$probeNode->getId()] = $probeNode;
            }
        }
        if (isset($data['contexts'])) {
            foreach ($data['contexts'] as $containerNodeData) {
                $contextOutline = new ContextOutline();
                $contextOutline->configureFromArray($containerNodeData);
                $this->addContextOutline($contextOutline);
            }
        }
        parent::configureFromArray($data);
    }

    /**
     * @param ContextOutline $contextOutline
     */
    public function addContextOutline(ContextOutline $contextOutline)
    {
        if ( ! $contextOutline->getId()) {
            //@todo throw exception
            return;
        }
        $this->contextOutlines[$contextOutline->getId()] = $contextOutline;
    }

    /**
     * @param string $id
     * @return bool
     */
    public function hasContextOutline(string $id = null):bool
    {
        if ( ! $id) {
            return count($this->contextOutlines) > 0;
        }
        return isset($this->contextOutlines[$id]);
    }

    /**
     * @param string $id
     * @return mixed|void
     */
    public function getContextOutline(string $id)
    {
        if ( ! $this->hasContextOutline($id)) {
            //@todo : throw exception
            return;
        }
        return $this->contextOutlines[$id];
    }

    /**
     * @return array
     */
    public function getContextOutlines(): array
    {
        return $this->contextOutlines;
    }

    /**
     * @return string
     */
    public function getWorkingDirectory(): string
    {
        return $this->workingDirectory;
    }

    /**
     * @param string $workingDirectory
     */
    public function setWorkingDirectory(string $workingDirectory)
    {
        $this->workingDirectory = $workingDirectory;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return array
     */
    public function getProbes(): array
    {
        $probes = $this->probes;
        if ($this->hasParent()) {
            $parent = $this->getParent();
            if ($parent instanceof SampleOutlineProvider) {
                $probes = $parent->getSampleOutline()->getProbes();
                self::mergeArrays($probes,$this->probes);
            }
        }
        return $probes;
    }

    /**
     * @return bool
     */
    public function hasProbe():bool
    {
        return count($this->probes) > 0;
    }

    /**
     * @param ContainerNode $probe
     */
    public function addProbe(ContainerNode $probe)
    {
        $this->probes[$probe->getId()] = $probe;
    }

    /**
     * @inheritDoc
     */
    public function getSampleOutline(): SampleOutline
    {
        $sampleOutline = new SampleOutline();
        $probes = $this->getProbes();
        foreach ($probes as $probe) {
            $sampleOutline->addProbe($probe);
        }
        if ($this->hasParent()){
            $parent = $this->getParent();
            if ($parent instanceof SampleOutlineProvider) {
                foreach ($parent->getSampleOutline()->getProbes() as $probe) {
                    $sampleOutline->addProbe($probe);
                }
            }
        }
        return $sampleOutline;
    }

    /**
     * @inheritDoc
     */
    public function toArray():array
    {
        $array = [];
        $array['name'] = $this->getName();
        if ( ! empty($this->workingDirectory)) {
            $array['workingDirectory'] = $this->getWorkingDirectory();
        }
        if ($this->hasProbe()) {
            $array['probes'] = [];
            foreach ($this->getProbes() as $probeId => $node) {
                $array['probes'][$probeId] = $node->jsonSerialize();
            }
        }
        if ($this->hasContextOutline()) {
            $array['contexts'] = [];
            foreach ($this->contextOutlines as $node) {
                $array['contexts'][] = $node->jsonSerialize();
            }
        }
        return $array;
    }
}