<?php
declare(strict_types = 1);
/**
 * Created by PhpStorm.
 * User: sophie.beaupuis
 * Date: 26/12/2016
 * Time: 09:02
 */

namespace Sophpie\Workbench\Project\Scan;


class ClassScanner
{
    /**
     * @var array
     */
    protected $methodInfos = [];

    /**
     * @param string $path
     * @return \OuterIterator
     */
    protected function getDirectoryIterator(string $path):\OuterIterator
    {
        $directoryIterator = new \RecursiveDirectoryIterator($path);
        $iterator = new \RecursiveIteratorIterator($directoryIterator);
        return new \RegexIterator($iterator, '/^.+\.php$/i',\RecursiveRegexIterator::GET_MATCH);
    }

    /**
     * @param string $path
     */
    public function scan(string $path):array
    {
        //@todo : adding cache for this !!!
        $classInfoCollection = [];
        foreach ($this->getDirectoryIterator($path) as $filePath => $object)
        {
            $fileResource = fopen($filePath,'r');
            $className = '';
            $namespace = '\\';
            while($codeLine = fgets($fileResource)) {
                $this->analyzeCodeLine($codeLine,$namespace,$className);
                if ( ! empty($className)) {
                    $this->analyzeClass($namespace,$className,$filePath,$classInfoCollection);
                    break;
                }
            }
        }
        return $classInfoCollection;
    }

    /**
     * @param string $codeLine
     * @param string $currentNamespace
     * @param string $currentClassName
     */
    protected function analyzeCodeLine(string $codeLine,string &$currentNamespace,string &$currentClassName)
    {
        if (preg_match('@^(?<isAbstract>[abstract|\s])*class[\s]+(?<className>[\w]+)@',$codeLine,$matches) > 0){
            $currentClassName = $matches['className'];
            //@todo : manage abstract class
        }
        if (preg_match('@namespace (?<namespace>[\w|\\\]+)@',$codeLine,$matches) > 0) {
            $currentNamespace = $matches['namespace'];
        }
    }

    /**
     * @param string $namespace
     * @param string $className
     * @param string $filePath
     * @param array $classInfoCollection
     */
    protected function analyzeClass(string $namespace,string $className,string $filePath,array &$classInfoCollection)
    {
        $fqn = $namespace.'\\'.$className;;
        if ( ! class_exists($fqn,false)) {
            include $filePath;
        }
        $classInfoCollection[$fqn] = ClassInfo::fromReflexionClass(new \ReflectionClass($fqn));
    }



}