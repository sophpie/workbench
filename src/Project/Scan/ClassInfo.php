<?php
declare(strict_types = 1);
/**
 * Created by PhpStorm.
 * User: sophie.beaupuis
 * Date: 26/12/2016
 * Time: 11:17
 */

namespace Sophpie\Workbench\Project\Scan;


use Sophpie\Workbench\Test\Sample\SampleOutline;

class ClassInfo
{
    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $namespace;

    /**
     * @var array
     */
    protected $methods = [];

    /**
     * ClassInfo constructor.
     * @param string $name
     * @param string $namespace
     */
    public function __construct(string $name, string $namespace)
    {
        $this->name = $name;
        $this->namespace = $namespace;
    }

    /**
     * @param MethodInfo $methodInfo
     */
    public function addMethod(MethodInfo $methodInfo)
    {
        $this->methods[$methodInfo->getName()] = $methodInfo;

    }

    /**
     * @param string $methodName
     * @return MethodInfo
     */
    public function getMethodInfo(string $methodName):MethodInfo
    {
        return $this->methods[$methodName];
    }

    /**
     * @param \ReflectionClass $refClass
     * @return ClassInfo
     */
    public static function fromReflexionClass(\ReflectionClass $refClass)
    {
        $classInfo = new self($refClass->getName(),$refClass->getNamespaceName());
        foreach ($refClass->getMethods() as $refMethod) {
            $methodInfo = MethodInfo::fromReflection($refMethod);
            $methodInfo->setClassInfo($classInfo);
            $classInfo->addMethod($methodInfo);
        }
        return $classInfo;
    }

    /**
     * @return string
     */
    public function getFullQualifiedName():string
    {
        return $this->name;
    }

    /**
     * @param string $methodName
     * @return SampleOutline
     */
    public function getSampleOutline(string $methodName):SampleOutline
    {
        $methodInfo = $this->getMethodInfo($methodName);
        return $methodInfo->getSampleOutline();
    }


}