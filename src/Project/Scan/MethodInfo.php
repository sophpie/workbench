<?php
declare(strict_types = 1);
/**
 * Created by PhpStorm.
 * User: sophie.beaupuis
 * Date: 26/12/2016
 * Time: 11:31
 */

namespace Sophpie\Workbench\Project\Scan;


use Sophpie\Workbench\Test\Sample\SampleOutline;

class MethodInfo
{
    const SCOPE_PRIVATE = 'private';
    const SCOPE_PUBLIC = 'public';
    const SCOPE_PROTECTED = 'protected';

    /**
     * @var string
     */
    protected $name;

    /**
     * @var array
     */
    protected $parameters = [];

    /**
     * @var string
     */
    protected $scope = self::SCOPE_PUBLIC;

    /**
     * @var ClassInfo
     */
    protected $classInfo;

    /**
     * MethodInfo constructor.
     * @param string $name
     */
    public function __construct($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getScope(): string
    {
        return $this->scope;
    }

    /**
     * @param string $scope
     */
    public function setScope(string $scope)
    {
        $this->scope = $scope;
    }

    /**
     * @param string $type
     * @param int|null $position
     */
    public function addParameter(string $type,int $position = null)
    {
        if ( ! $position) $position = count($this->parameters);
        $this->parameters[$position] = $type;
    }

    /**
     * @param \ReflectionMethod $reflectionMethod
     * @return MethodInfo
     */
    public static function fromReflection(\ReflectionMethod $reflectionMethod)
    {
        $methodInfo = new self($reflectionMethod->getName());
        if ($reflectionMethod->isPrivate()) $methodInfo->setScope(self::SCOPE_PRIVATE);
        if ($reflectionMethod->isProtected()) $methodInfo->setScope(self::SCOPE_PROTECTED);
        foreach ($reflectionMethod->getParameters() as $parameter) {
            $position = $parameter->getPosition();
            $type= $parameter->getType();
            $methodInfo->addParameter((string)$type,$position);
        }
        return $methodInfo;
    }

    /**
     * @return ClassInfo
     */
    public function getClassInfo(): ClassInfo
    {
        return $this->classInfo;
    }

    /**
     * @param ClassInfo $classInfo
     */
    public function setClassInfo(ClassInfo $classInfo)
    {
        $this->classInfo = $classInfo;
    }

    /**
     * @return string
     */
    public function getFullName():string
    {
        return $this->getClassInfo()->getFullQualifiedName() . '::' . $this->getName();
    }

    /**
     * @return SampleOutline
     */
    public function getSampleOutline():SampleOutline
    {
        $sampleDefinition = new SampleOutline();
        $sampleDefinition->setFunctionName($this->getName());
        return $sampleDefinition;

    }
}