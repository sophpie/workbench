<?php
declare(strict_types = 1);
/**
 * Created by PhpStorm.
 * User: sophie.beaupuis
 * Date: 30/12/2016
 * Time: 05:49
 */

namespace Sophpie\Workbench\DependencyInjection;


use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\DependencyInjection\Reference;

class ContainerNode implements \JsonSerializable
{

    const TYPE_REFERENCE = 'reference';
    const TYPE_DEFINITION = 'definition';
    const TYPE_CONSTANT = 'constant';

    /**
     * @var string
     */
    protected $type;

    /**
     * @var Definition|Reference
     */
    protected $internalNode;

    /**
     * @var array
     */
    protected $arrayData = [];

    /**
     * @var ContainerBuilder
     */
    protected $containerBuilder;

    /**
     * ContainerNode constructor.
     * @param $data
     * @param ContainerBuilder|null $containerBuilder
     */
    public function __construct($data,ContainerBuilder $containerBuilder = null)
    {
        $this->type = self::TYPE_CONSTANT;
        $this->arrayData = $data;
        $this->internalNode = $data;
        if (is_array($data) && isset($data['type'])) {
            $this->type = $data['type'];
            $this->parseData();
        }
        $this->containerBuilder = $containerBuilder;
    }

    /**
     * @return ContainerBuilder
     */
    public function getContainerBuilder(): ContainerBuilder
    {
        return $this->containerBuilder;
    }

    /**
     * @param ContainerBuilder $containerBuilder
     */
    public function setContainerBuilder(ContainerBuilder $containerBuilder)
    {
        $this->containerBuilder = $containerBuilder;
    }

    /**
     *
     */
    protected function parseData()
    {
        switch ($this->arrayData['type']) {
            case self::TYPE_DEFINITION : $this->parseAsDefinition(); break;
            case self::TYPE_REFERENCE: $this->parseAsReference(); break;
        }
    }

    /**
     *
     */
    protected function parseAsReference()
    {
        //@todo : throw exception if $this->type not TYPE_REFERENCE
        $this->internalNode = new Reference($this->arrayData['data']['serviceId']);
    }

    /**
     *
     */
    protected function parseAsDefinition()
    {
        //@todo : thorw exception if $this->type not TYPE_DEFINITION
        $definitionData = $this->arrayData['data'];
        if (isset($definitionData['arguments'])) {
            $args = [];
            foreach ($definitionData['arguments'] as $argData) {
                $node = new ContainerNode($argData,$this->containerBuilder);
                $args[] = $node->getInternalNode();
            }
            $this->internalNode = new Definition($definitionData['class'],$args);
        }
        else {
            $this->internalNode = new Definition($definitionData['class']);
        }
    }

    /**
     * @param $data
     * @param ContainerBuilder|null $containerBuilder
     * @return ContainerNode
     */
    public static function factory($data, ContainerBuilder $containerBuilder = null)
    {
        $node = new self($data,$containerBuilder);
        return $node;
    }

    /**
     * @return Definition|Reference
     */
    public function getInternalNode()
    {
        return $this->internalNode;
    }


    /**
     * @inheritDoc
     */
    public function jsonSerialize()
    {
        return $this->arrayData;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @return Definition
     * @internal param ContainerBuilder $container
     */
    public function getDefinition():Definition
    {
        //@todo:throw exception if $this->type not TYPE_REFERENCE
        if ($this->getType() == self::TYPE_REFERENCE) {
            $serviceId = (string)$this->getInternalNode();
            return $this->containerBuilder->getDefinition($serviceId);
        }
        if ($this->getType() == self::TYPE_DEFINITION) {
            return $this->getInternalNode();
        }
    }

    /**
     * @return string
     */
    public function getClassName():string
    {
        if ($this->getDefinition() instanceof Definition) {
            return $this->getDefinition()->getClass();
        }
    }

    /**
     * @param ContainerBuilder $container
     * @return bool|float|int|mixed|string|Definition|Reference
     */
    public function resolveWith(ContainerBuilder $container)
    {
        if ($this->type ==self::TYPE_CONSTANT) return $this->internalNode;
        return $container->resolveServices($this->internalNode);
    }

    /**
     * @return string
     */
    public function getId():string
    {
        switch($this->getType()) {
            case self::TYPE_DEFINITION: return $this->getInternalNode()->getClass();
            case self::TYPE_REFERENCE: return 'ref:' . (string)$this->getInternalNode();
        }
        //@todo and what about constant ?
    }


}