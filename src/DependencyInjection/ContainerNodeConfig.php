<?php
declare(strict_types = 1);
/**
 * Created by PhpStorm.
 * User: sophie.beaupuis
 * Date: 23/12/2016
 * Time: 14:02
 */

namespace Sophpie\Workbench\DependencyInjection;


use Symfony\Component\Config\Definition\ConfigurationInterface;

class ContainerNodeConfig implements ConfigurationInterface
{
    /**
     * @inheritDoc
     */
    public function getConfigTreeBuilder()
    {
    }

}