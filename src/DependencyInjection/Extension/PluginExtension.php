<?php
declare(strict_types = 1);

/**
 * Created by PhpStorm.
 * User: sophie.beaupuis
 * Date: 02/01/2017
 * Time: 09:18
 */
namespace Sophpie\Workbench\DependencyInjection\Extension;

use Sophpie\Workbench\DependencyInjection\ContainerNode;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Extension\ExtensionInterface;

class PluginExtension implements ExtensionInterface
{
    /**
     * @inheritDoc
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        foreach ($configs[0] as $pluginName => $pluginData) {
            $options = null;
            if (isset($pluginData['options'])) {
                $options= $pluginData['options'];
            }
            $pluginNode = ContainerNode::factory($pluginData['node'],$container);
            if (is_array($options)) {
                $pluginDefinition = new Definition($pluginNode->getClassName(),[$options]);
            }
            else $pluginDefinition = new Definition($pluginNode->getClassName());
            $alias = 'plugin.' . $pluginName;
            $container->setAlias($alias,$pluginNode->getClassName());
            $pluginDefinition->addTag('workbench.plugin');
            $container->setDefinition($pluginNode->getClassName(),$pluginDefinition);
        }
    }

    /**
     * @inheritDoc
     */
    public function getNamespace()
    {
        return 'plugins';
    }

    /**
     * @inheritDoc
     */
    public function getXsdValidationBasePath()
    {
        // TODO: Implement getXsdValidationBasePath() method.
    }

    /**
     * @inheritDoc
     */
    public function getAlias()
    {
        return $this->getNamespace();
    }

}