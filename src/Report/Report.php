<?php
declare(strict_types = 1);
/**
 * Created by PhpStorm.
 * User: sophie.beaupuis
 * Date: 26/01/2017
 * Time: 08:54
 */

namespace Sophpie\Workbench\Report;


use Sophpie\Workbench\Test\Outline\OutlineAbstract;
use Sophpie\Workbench\Test\Result\Result;

/**
 * Class Report
 * @package Sophpie\Workbench\Report
 */
class Report extends OutlineAbstract
{
    const ID = 'id';
    const RESULTS = 'results';

    /**
     * @var string
     */
    protected $id;

    /**
     * @var array
     */
    protected $results = [];

    /**
     * @inheritDoc
     */
    public function configureFromArray(array $data)
    {
        parent::configureFromArray($data);
        if (isset($data[self::ID])) {
            $this->id = $data[self::ID];
        }
        if (isset($data[self::RESULTS])) {
            foreach ($data[self::RESULTS] as $resultData) {
                $result = new Result();
                $result->configureFromArray($resultData);
            }
        }

    }

    /**
     * @param string $id
     */
    public function setId(string $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param Result $result
     */
    public function addResult(Result $result)
    {
        $this->results[] = $result;
    }


    /**
     * @inheritDoc
     */
    public function toArray(): array
    {
        $array = [];
        if ($this->id) {
            $array[self::ID] = $this->id;
        }
        if (count($this->results) > 0) {
            foreach ($this->results as $result) {
                $array[self::RESULTS][] = $result->toArray();
            }
        }
        return $array;
    }

    /**
     * @return array
     */
    public function getResults(): array
    {
        return $this->results;
    }
}