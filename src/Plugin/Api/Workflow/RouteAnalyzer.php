<?php
declare(strict_types = 1);
/**
 * Created by PhpStorm.
 * User: sophie.beaupuis
 * Date: 22/12/2016
 * Time: 11:26
 */

namespace Sophpie\Workbench\Plugin\Api\Workflow;


use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Routing\Router;

class RouteAnalyzer implements EventSubscriberInterface
{
    /**
     * @var Router
     */
    protected $router;

    /**
     * RouteAnalyzer constructor.
     * @param Router $router
     */
    public function __construct(Router $router)
    {
        $this->router = $router;
    }

    /**
     * @param GetResponseEvent $event
     */
    public function onKernelRequest(GetResponseEvent $event)
    {
        $request = $event->getRequest();
        $parameters = $this->router->matchRequest($request);
        $request->attributes->add($parameters);
        //@todo : exception if no route found
    }

    /**
     * @inheritDoc
     */
    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::REQUEST => ['onKernelRequest',32]
        ];
    }

}