<?php
declare(strict_types = 1);
/**
 * Created by PhpStorm.
 * User: sophie.beaupuis
 * Date: 04/01/2017
 * Time: 14:33
 */

namespace Sophpie\Workbench\Plugin\Api\Workflow;


use Sophpie\Workbench\Plugin\Api\TestResult;
use Sophpie\Workbench\Test\Result\Result;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\GetResponseForControllerResultEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Class JsonRenderer
 * @package Sophpie\Workbench\Plugin\Api\Workflow
 */
class JsonRenderer implements EventSubscriberInterface
{
    /**
     * @param GetResponseForControllerResultEvent $event
     */
    public function jsonRender(GetResponseForControllerResultEvent $event)
    {
        $result = $event->getControllerResult();
        if ($result instanceof Response) return;
        $request = $event->getRequest();
        if ($request->headers->get('Content-type') != 'application/json') return;
        if ($result instanceof Result) {
            $result = new TestResult($result);
        }
        $response = new Response(json_encode($result,JSON_PRETTY_PRINT|JSON_FORCE_OBJECT),200,['Content-type' => 'application/json']);
        $event->setResponse($response);
    }

    /**
     * @inheritDoc
     */
    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::VIEW => ['jsonRender',0],
        ];
    }

}