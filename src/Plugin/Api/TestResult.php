<?php
declare(strict_types = 1);
/**
 * Created by PhpStorm.
 * User: sophie.beaupuis
 * Date: 04/01/2017
 * Time: 15:01
 */

namespace Sophpie\Workbench\Plugin\Api;

use Sophpie\Workbench\Test\Result\Result;

/**
 * Class TestResult
 * @package Sophpie\Workbench\Plugin\Api
 */
class TestResult implements \JsonSerializable
{
    /**
     * @var Result
     */
    protected $result;

    /**
     * TestResult constructor.
     * @param Result $result
     */
    public function __construct(Result $result)
    {
        $this->result = $result;
    }

    /**
     * @return Result
     */
    public function getResult(): Result
    {
        return $this->result;
    }

    /**
     * @param Result $result
     */
    public function setResult(Result $result)
    {
        $this->result = $result;
    }

    /**
     * @param string $json
     */
    public function fromJson(string $json)
    {
        $data = json_decode($json,true);
        $this->result->setSuccess((bool)$data['isSuccess']);
        $this->result->setExecutionResult($data['executionResult']);
        foreach ($data['probeResults'] as $probeName => $probeData) {
            $this->result->addProbeResult($probeName,$probeData);
        }
        foreach ($data['assertions'] as $assertionName => $assertionData) {
            foreach ($assertionData as $result) {
                $this->result->addAssertionResult($assertionName,$result['dataPath'],$result['value'],$result['isSuccess']);
            }
        }
    }

    /**
     * @inheritDoc
     */
    function jsonSerialize()
    {
        $jsonArray = [
            'isSuccess' => $this->result->isSuccess(),
            'executionResult' => $this->result->getExecutionResult(),
        ];
        $jsonArray['probeResults'] = [];
        foreach ($this->result->getProbeResults() as $probeName => $probeResult) {
            $jsonArray['probes'][$probeName] = $probeResult;
        }
        $jsonArray['assertions'] = [];
        foreach ($this->result->getAssertionResults() as $assertionName => $assertionResults) {
            foreach ($assertionResults as $assertionResult) {
                if ( ! isset($jsonArray['assertions'][$assertionName])) {
                    $jsonArray['assertions'][$assertionName] = [];
                }
                $jsonArray['assertions'][$assertionName][] = $assertionResult;
            }
        }
        return $jsonArray;

    }


}