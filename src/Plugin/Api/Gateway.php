<?php
declare(strict_types = 1);
/**
 * Created by PhpStorm.
 * User: sophie.beaupuis
 * Date: 21/12/2016
 * Time: 15:46
 */

namespace Sophpie\Workbench\Plugin\Api;


use Sophpie\Workbench\Plugin\Api\Workflow\ControllerBuilder;
use Sophpie\Workbench\Plugin\Api\Workflow\JsonRenderer;
use Sophpie\Workbench\Plugin\Api\Workflow\RouteAnalyzer;
use Sophpie\Workbench\Plugin\Api\Workflow\TestRequestDataExtractor;
use Sophpie\Workbench\Workbench;
use Symfony\Component\Config\Definition\Processor;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\Config\Loader\LoaderInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Reference;
use Symfony\Component\EventDispatcher\ContainerAwareEventDispatcher;
use Symfony\Component\EventDispatcher\DependencyInjection\RegisterListenersPass;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Controller\ControllerResolver;
use Symfony\Component\HttpKernel\HttpKernel;
use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Routing\Loader\PhpFileLoader;
use Symfony\Component\Routing\Router;

class Gateway extends Kernel
{
    /**
     * @var string
     */
    protected $applicationConfigPath;

    /**
     * @inheritDoc
     */
    public function __construct($environment, $debug, string $applicationConfigPath)
    {
        parent::__construct($environment, $debug);
        $this->applicationConfigPath = $applicationConfigPath;
    }


    /**
     * @inheritDoc
     */
    public function registerBundles()
    {
        return [];
    }

    /**
     * Build workflow subscriber definitions
     * @param ContainerBuilder $container
     * @return array
     */
    protected function buildWorkflowUnits(ContainerBuilder $container)
    {
        $workflowUnitDefinitions = [];
        $workflowUnitDefinitions['test_request_data_extractor'] = new Definition(TestRequestDataExtractor::class);
        $workflowUnitDefinitions['jsonRenderer'] = new Definition(JsonRenderer::class);
        $workflowUnitDefinitions['controllerBuilder'] = new Definition(ControllerBuilder::class,[new Reference('service_container')]);
        foreach ($workflowUnitDefinitions as $unit) {
            $unit->addTag('kernel.event_subscriber');
        }
        $container->addDefinitions($workflowUnitDefinitions);
    }

    /**
     * @inheritDoc
     */
    public function registerContainerConfiguration(LoaderInterface $loader)
    {
    }

    /**
     * @return string
     */
    public function getApplicationConfigPath(): string
    {
        return $this->applicationConfigPath;
    }

    /**
     * @inheritDoc
     */
    protected function getContainerBuilder()
    {
        $containerBuilder = parent::getContainerBuilder();
        return $containerBuilder;
    }


    /**
     * Build router service definitions
     * @param ContainerBuilder $container
     */
    protected function buildRouter(ContainerBuilder $container)
    {
        $routerDefinition = new Definition(RouteAnalyzer::class,[
            new Definition(Router::class,
                [
                    new Definition(PhpFileLoader::class,[
                        new Definition(FileLocator::class,[
                            [__DIR__ . '/config']])]),
                    'routes.php'
                ]
            ),

        ]);
        $routerDefinition->addTag('kernel.event_subscriber');
        $container->addDefinitions(['router' => $routerDefinition]);
    }

    /**
     * @return \Symfony\Component\DependencyInjection\ContainerBuilder
     */
    protected function buildContainer()
    {
        $container  =  parent::buildContainer();
        $container->addCompilerPass(new RegisterListenersPass());
        $this->buildRouter($container);
        $this->buildWorkflowUnits($container);
        $container->addDefinitions(
            [
                'event_dispatcher' => new Definition(ContainerAwareEventDispatcher::class,
                    [
                        new Reference('service_container')
                    ]
                ),
                'controller_resolver' => new Definition(ControllerResolver::class),
                'request_stack'=> new Definition(RequestStack::class),
                'http_kernel' => new Definition(HttpKernel::class,
                    [
                        new Reference('event_dispatcher'),
                        new Reference('controller_resolver'),
                        new Reference('request_stack'),
                    ]
                ),
                'config_processor' => new Definition(Processor::class),
            ]
        );
        $workbenchDefinition = new Definition(Workbench::class,[$this->getApplicationConfigPath()]);
        $container->setDefinition('workbench',$workbenchDefinition);
        return $container;
    }

    /**
     * @inheritDoc
     */
    public function getCacheDir()
    {
        return __DIR__ . '/../../var/cache';
    }


}