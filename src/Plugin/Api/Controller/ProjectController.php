<?php
declare(strict_types = 1);
/**
 * Created by PhpStorm.
 * User: sophie.beaupuis
 * Date: 26/12/2016
 * Time: 09:22
 */

namespace Sophpie\Workbench\Plugin\Api\Controller;


use Sophpie\Workbench\Project\Project;
use Symfony\Component\HttpFoundation\Response;

class ProjectController extends ApiControllerAbstract
{
    /**
     * @param string $id
     * @return Response
     */
    public function getAction(string $id)
    {
        $repo = $this->getRepository('repository.projectRepository');
        $project = $repo->get($id);
        return new Response(json_encode($project,JSON_FORCE_OBJECT | JSON_PRETTY_PRINT),200,['Content-type' => 'application/json']);
    }

    /**
     * @param array $jsonData
     * @return Response
     */
    public function setupAction(array $jsonData)
    {
        $project = new Project();
        $project->configureFromArray($jsonData);
        $repo = $this->getRepository('repository.projectRepository');
        $repo->insert($project);
        return new Response('new project',200,['Content-type' => 'application/json']);
    }





}