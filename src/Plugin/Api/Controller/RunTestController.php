<?php
declare(strict_types = 1);
/**
 * Created by PhpStorm.
 * User: sophie.beaupuis
 * Date: 22/12/2016
 * Time: 08:27
 */

namespace Sophpie\Workbench\Plugin\Api\Controller;


use Sophpie\Workbench\Plugin\Api\TestResult;
use Sophpie\Workbench\Test\Result\Result;
use Sophpie\Workbench\Test\Result\ResultCollection;
use Sophpie\Workbench\Test\TestSuite\TestSuite;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class RunTestController
 * @package Sophpie\Workbench\Plugin\Api\Controller
 */
class RunTestController implements ContainerAwareInterface
{
    use ContainerAwareTrait;
    //@todo : use ArgumentResolve from Http Kernel ?

    /**
     * @param string $functionName
     * @param string|null $className
     * @param array $jsonData
     * @return Response
     */
    public function runUrlTestAction(string $functionName,string $className = null,array $jsonData)
    {

    }

    /**
     * @param array $jsonData
     * @return Response
     */
    public function runTestAction(array $jsonData)
    {
        $workbench = $this->container->get('workbench');
        return $workbench->run($jsonData);
    }

    /**
     * @param array $jsonData
     * @return Response
     */
    public function runTestSuiteAction(array $jsonData)
    {
        $testSuite = new TestSuite();
        $testSuite->configureFromArray($jsonData);
        $curlHandle = array();
        $multiHandle = curl_multi_init();
        //var_dump($testSuite);
        foreach ($testSuite->getTestOutlines() as $i => $testData) {
            $curlHandle[$i] = curl_init();
            //@todo : dela with host name and docker
            curl_setopt($curlHandle[$i], CURLOPT_URL,'http://nginx:80/run/foo');
            //@todo : make configurable curl options
            curl_setopt($curlHandle[$i], CURLOPT_HEADER,0);
            curl_setopt($curlHandle[$i], CURLOPT_RETURNTRANSFER,1);
            curl_setopt($curlHandle[$i], CURLOPT_POST,1);
            curl_setopt($curlHandle[$i], CURLOPT_HTTPHEADER,array('Content-Type: application/json'));
            curl_setopt($curlHandle[$i], CURLOPT_CONNECTTIMEOUT_MS,200);
            curl_setopt($curlHandle[$i], CURLOPT_TIMEOUT_MS,300);
            //var_dump($testData);
            $body = json_encode($testData);
            curl_setopt($curlHandle[$i], CURLOPT_POSTFIELDS, $body);
            curl_multi_add_handle($multiHandle, $curlHandle[$i]);
        }
        $running = null;
        do {
            $status = curl_multi_exec($multiHandle, $running);
            $info = curl_multi_info_read($multiHandle);
            //@todo : make a better treatment of curl Error
            if (false !== $info) {
                if ($info['result'] != CURLE_OK) {
                    //@todo : throw exception
                }
            }
            if($status > 0) {
                echo "ERROR!\n " . curl_multi_strerror($status);
            }
        } while($status === CURLM_CALL_MULTI_PERFORM || $running > 0);
        $resultCollection = new ResultCollection();
        foreach($curlHandle as $id => $c) {
            $resultJsonContent= curl_multi_getcontent($c);
            $result = new Result();
            $testResult = new TestResult($result);
            $testResult->fromJson($resultJsonContent);
            $resultCollection->addResult($testResult->getResult());
            curl_multi_remove_handle($multiHandle, $c);
        }
        curl_multi_close($multiHandle);
        return $resultCollection;
    }
}