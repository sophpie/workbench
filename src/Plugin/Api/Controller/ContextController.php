<?php
declare(strict_types = 1);
/**
 * Created by PhpStorm.
 * User: sophie.beaupuis
 * Date: 03/01/2017
 * Time: 10:46
 */

namespace Sophpie\Workbench\Plugin\Api\Controller;


use Sophpie\Workbench\Test\Context\ContextOutline;
use Symfony\Component\HttpFoundation\Response;

class ContextController extends ApiControllerAbstract
{
    public function set($jsonData)
    {
        $contextOutline = new ContextOutline();
        $contextOutline->configureFromArray($jsonData);
        $repo = $this->getRepository('repository.contextOutlineRepository');
        $repo->insert($contextOutline);
        return new Response('new contextOutline',200,['Content-type' => 'application/json']);
    }

    /**
     * @param $id
     * @return Response
     */
    public function get($id)
    {
        $repo = $this->getRepository('repository.contextOutlineRepository');
        $object = $repo->get($id);
        return new Response(json_encode($object,JSON_FORCE_OBJECT | JSON_PRETTY_PRINT),200,['Content-type' => 'application/json']);
    }
}