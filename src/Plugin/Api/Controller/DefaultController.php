<?php
declare(strict_types = 1);
/**
 * Created by PhpStorm.
 * User: sophie.beaupuis
 * Date: 22/12/2016
 * Time: 08:25
 */

namespace Sophpie\Workbench\Plugin\Api\Controller;


use Symfony\Component\HttpFoundation\Response;

class DefaultController
{
    public function IndexAction()
    {
        return new Response('rien',200);
    }
}