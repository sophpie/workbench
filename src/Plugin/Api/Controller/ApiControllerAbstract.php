<?php
declare(strict_types = 1);
/**
 * Created by PhpStorm.
 * User: sophie.beaupuis
 * Date: 30/12/2016
 * Time: 16:10
 */

namespace Sophpie\Workbench\Plugin\Api\Controller;


use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Class ApiControllerAbstract
 * @package Sophpie\Workbench\Plugin\Api\Controller
 */
abstract class ApiControllerAbstract implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * @param $repositoryAlias
     * @return mixed
     */
    protected function getRepository($repositoryAlias)
    {
        $workbench = $this->container->get('workbench');
        return $workbench->getContainer()->get($repositoryAlias);
    }
}