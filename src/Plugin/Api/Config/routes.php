<?php
declare(strict_types = 1);
/**
 * Created by PhpStorm.
 * User: sophie.beaupuis
 * Date: 23/12/2016
 * Time: 08:07
 */
use Sophpie\Workbench\Plugin\Api\Controller\ContextController;
use Sophpie\Workbench\Plugin\Api\Controller\DefaultController;
use Sophpie\Workbench\Plugin\Api\Controller\ProjectController;
use Sophpie\Workbench\Plugin\Api\Controller\RunTestController;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

$routeCollection = new RouteCollection();
$routeCollection->add('default',new Route('/',[
    '_controller' => DefaultController::class . '::indexAction',
]));
$routeCollection->add('testRun',new Route('/run/{name}',[
    '_controller' => RunTestController::class . '::runTestAction',
]));
$routeCollection->add('testSuiteRun',new Route('/runSuite/{name}',[
    '_controller' => RunTestController::class . '::runTestSuiteAction',
]));
$routeCollection->add('projectRoute',new Route('/project',[
    '_controller'=>ProjectController::class . '::scanProjectAction'
]));
//Project
$routeCollection->add('setProject',new Route('/project/setup',[
    '_controller'=>ProjectController::class . '::setupAction'
]));
$routeCollection->add('listProject',new Route('/project/list',[
    '_controller'=>ProjectController::class . '::listAction'
]));
$routeCollection->add('getProject',new Route('/project/{id}',[
    '_controller'=>ProjectController::class . '::getAction'
]));
//Context
$routeCollection->add('getContext',new Route('/context',[
    '_controller'=>ContextController::class . '::set'
    ]
));
$routeCollection->add('insertContext',new Route('/context/{id}',[
    '_controller'=>ContextController::class . '::get'
    ]
));
return $routeCollection;