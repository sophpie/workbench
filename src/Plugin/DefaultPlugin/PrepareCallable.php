<?php
declare(strict_types = 1);
/**
 * Created by PhpStorm.
 * User: sophie.beaupuis
 * Date: 29/12/2016
 * Time: 14:54
 */

namespace Sophpie\Workbench\Plugin\DefaultPlugin;


use Sophpie\Workbench\Test\TestEvent;
use Sophpie\Workbench\Test\TestManager;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class PrepareCallable implements EventSubscriberInterface
{
    public function prepareCallable(TestEvent $event)
    {
        $sample = $event->getTest()->getSample();
        $context = $event->getTest()->getContext();
        $callable = null;
        if ($context->hasInstance()) {
            $instance = $context->getInstance();
            $className = get_class($instance);
            $refClass = new \ReflectionClass($className);
            $refMethod = $refClass->getMethod($sample->getFunctionName());
            $callable = [$instance,$sample->getFunctionName()];
        }
        else {
            //@todo : case of simple function
        }
        $event->setCallable($callable);
        //@todo : deal with private and protected method by reflection.
    }

    /**
     * @inheritDoc
     */
    public static function getSubscribedEvents()
    {
        return [
            TestManager::EVENT_PREPARE => ['prepareCallable',0],
        ];
    }


}