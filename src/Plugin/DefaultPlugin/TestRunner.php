<?php
declare(strict_types = 1);
/**
 * Created by PhpStorm.
 * User: sophie.beaupuis
 * Date: 20/12/2016
 * Time: 13:13
 */

namespace Sophpie\Workbench\Plugin\DefaultPlugin;


use Sophpie\Workbench\Test\TestEvent;
use Sophpie\Workbench\Test\TestManager;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class TestRunner
 * @package Sophpie\Workbench\Test\Listener
 */
class TestRunner implements EventSubscriberInterface
{

    /**
     * @param TestEvent $event
     */
    public function onRun(TestEvent $event)
    {
        $args = $event->getCallableArguments();
        $callable = $event->getCallable();
        try {
            $result = call_user_func_array($callable,$args);
            $event->getResult()->setExecutionResult($result);
        }
        catch (\Exception $e) {}
    }

    /**
     * @inheritDoc
     */
    public static function getSubscribedEvents()
    {
        return [
            TestManager::EVENT_RUN => ['onRun',0]
        ];
    }

}