<?php
declare(strict_types = 1);
/**
 * Created by PhpStorm.
 * User: sophie.beaupuis
 * Date: 20/12/2016
 * Time: 14:58
 */

namespace Sophpie\Workbench\Plugin\DefaultPlugin;


use Sophpie\Workbench\Assertion\AssertionTest;
use Sophpie\Workbench\Test\TestEvent;
use Sophpie\Workbench\Test\TestManager;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class AssertionResolver
 * @package Sophpie\Workbench\Plugin\DefaultPlugin
 */
class AssertionResolver implements EventSubscriberInterface
{
    /**
     * @var ContainerBuilder
     */
    protected $containerBuilder;

    /**
     * SetPointResolver constructor.
     * @param ContainerBuilder $containerBuilder
     */
    public function __construct(ContainerBuilder $containerBuilder)
    {
        $this->containerBuilder = $containerBuilder;
    }

    /**
     * @param TestEvent $event
     */
    public function onPostRun(TestEvent $event)
    {
        $context = $event->getTest()->getContext();
        foreach ($context->getAssertionTests() as $assertionTest) {
            /** @var AssertionTest $assertionTest */
            $assertion = $assertionTest->getAssertion()->resolveWith($this->containerBuilder);
            $assertionTest->setAssertion($assertion);
            $expectedValue = $assertionTest->getExpectedValue()->resolveWith($this->containerBuilder);
            $assertionTest->setExpectedValue($expectedValue);
        }
    }

    /**
     * @inheritDoc
     */
    public static function getSubscribedEvents()
    {
        return [
            TestManager::EVENT_POST_RUN => ['onPostRun',1000],
        ];
    }

}