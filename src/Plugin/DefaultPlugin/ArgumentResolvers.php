<?php
declare(strict_types = 1);
/**
 * Created by PhpStorm.
 * User: sophie.beaupuis
 * Date: 20/12/2016
 * Time: 11:48
 */

namespace Sophpie\Workbench\Plugin\DefaultPlugin;


use Sophpie\Workbench\Test\TestEvent;
use Sophpie\Workbench\Test\TestManager;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class ArgumentResolvers implements EventSubscriberInterface
{
    /**
     * @var ContainerBuilder
     */
    protected $containerBuilder;

    /**
     * ArgumentResolvers constructor.
     * @param ContainerBuilder $container
     */
    public function __construct(ContainerBuilder $container)
    {
        $this->containerBuilder = $container;
    }


    /**
     * @param TestEvent $event
     */
    public function onPrepare(TestEvent $event)
    {
        $context = $event->getTest()->getContext();
        $args = [];
        foreach ($context->getArguments() as $position =>$arguments) {
            $args[] = $this->containerBuilder->resolveServices($arguments);
        }
        $event->setCallableArguments($args);
    }

    /**
     * @inheritDoc
     */
    public static function getSubscribedEvents()
    {
        return [
            TestManager::EVENT_PREPARE => ['onPrepare',0],
        ];
    }

}