<?php
declare(strict_types = 1);
/**
 * Created by PhpStorm.
 * User: sophie.beaupuis
 * Date: 20/12/2016
 * Time: 15:47
 */

namespace Sophpie\Workbench\Plugin\DefaultPlugin;


use Sophpie\Workbench\Assertion\AssertionTest;
use Sophpie\Workbench\Test\DataMiner\DataMiner;
use Sophpie\Workbench\Test\TestEvent;
use Sophpie\Workbench\Test\TestManager;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class AssertionChecker implements EventSubscriberInterface
{
    /**
     * @var DataMiner
     */
    protected $dataMiner;

    /**
     * AssertionChecker constructor.
     * @param DataMiner $dataMiner
     */
    public function __construct(DataMiner $dataMiner)
    {
        $this->dataMiner = $dataMiner;
    }

    /**
     * @param TestEvent $event
     */
    public function onPostRun(TestEvent $event)
    {
        $context = $event->getTest()->getContext();
        foreach ($context->getAssertionTests() as $test) {
            /* @var $test AssertionTest */
            $this->dataMiner->setTestEvent($event);
            $left = $this->dataMiner->findData($test->getRealValueDataPath());
            $assertion = $test->getAssertion();
            //@todo throw exception if $assertion is not a Assertion object (not resolved service)
            $isSuccess = $assertion->isValid($left, $test->getExpectedValue());
            if ( ! $isSuccess) {
                $event->getResult()->setSuccess(false);
            }
            $event->getResult()->addAssertionResult(
                get_class($assertion),
                $test->getRealValueDataPath(),
                $test->getExpectedValue(),
                $isSuccess
            );
        }
    }

    /**
     * @inheritDoc
     */
    public static function getSubscribedEvents()
    {
        return [
            TestManager::EVENT_POST_RUN => ['onPostRun',800],
        ];
    }

}