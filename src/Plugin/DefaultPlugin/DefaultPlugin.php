<?php
declare(strict_types = 1);
/**
 * Created by PhpStorm.
 * User: sophie.beaupuis
 * Date: 02/01/2017
 * Time: 11:14
 */

namespace Sophpie\Workbench\Plugin\DefaultPlugin;


use Sophpie\Workbench\Plugin\PluginAbstract;
use Sophpie\Workbench\Workbench;
use Sophpie\Workbench\WorkbenchEvent;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Class DefaultPlugin
 * @package Sophpie\Workbench\Plugin\DefaultPlugin
 */
class DefaultPlugin extends PluginAbstract
{
    /**
     * @param WorkbenchEvent $event
     */
    public function configureContainer(WorkbenchEvent $event)
    {
        $containerBuilder = $event->getContainerBuilder();
        $workflowSubscriberDefinitions = [
            'argumentsResolver' => new Definition(ArgumentResolvers::class,[new Reference('service_container')]),
            'assertionResolver' => new Definition(AssertionResolver::class,[new Reference('service_container')]),
            'assertionChecker' => new Definition(AssertionChecker::class,[new Reference('dataMiner')]),
            'prepareCallable' => new Definition(PrepareCallable::class),
            'testRunner' => new Definition(TestRunner::class),

        ];
        $containerBuilder->addDefinitions($workflowSubscriberDefinitions);
        foreach ($workflowSubscriberDefinitions as $definition) {
            $definition->addTag('workbench.subscriber');
        }
    }

    /**
     * @inheritDoc
     */
    public static function getSubscribedEvents()
    {
        return [
            Workbench::EVENT_EXTENSIONS_LOADING => ['configureContainer',0]
        ];
    }

}