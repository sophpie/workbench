<?php
declare(strict_types = 1);
/**
 * Created by PhpStorm.
 * User: sophie.beaupuis
 * Date: 29/12/2016
 * Time: 08:20
 */

namespace Sophpie\Workbench\Plugin\PersistencePlugin;


use MongoDB\Driver\Manager;
use Sophpie\Workbench\Plugin\PluginAbstract;
use Sophpie\Workbench\Workbench;
use Sophpie\Workbench\WorkbenchEvent;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Reference;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class PersistencePlugin extends PluginAbstract implements EventSubscriberInterface
{
    /**
     * @param WorkbenchEvent $event
     */
    public function configureContainer(WorkbenchEvent $event)
    {
        $options = $this->getOptions();
        $containerBuilder = $event->getContainerBuilder();
        $mongoDbManagerDefinition = new Definition(Manager::class,[$options['mongoDbServer']]);
        $containerBuilder->setDefinition('mongoDbManager',$mongoDbManagerDefinition);
        $containerBuilder->setDefinition('repositoryManager',new Definition(RepositoryManager::class,[new Reference('service_container')]));
        if (isset($options['repositories'])) {
            foreach ($options['repositories'] as $repositoryClassName) {
                $containerBuilder->setDefinition($repositoryClassName,
                    new Definition($repositoryClassName,[new Reference('mongoDbManager'),new Reference('repositoryManager')])
                );
                $alias = $this->getAliasName($repositoryClassName);
                $containerBuilder->setAlias($alias,$repositoryClassName);
            }
        }
    }

    /**
     * @param $repositoryClassName
     * @return string
     */
    protected function getAliasName($repositoryClassName):string
    {
        $className = current(array_reverse(explode("\\",$repositoryClassName)));
        $aliasName = "repository." . $className;
        return $aliasName;
    }

    /**
     * @inheritDoc
     */
    public static function getSubscribedEvents()
    {
        return [
            Workbench::EVENT_EXTENSIONS_LOADING => ['configureContainer',0]
        ];
    }


}