<?php
declare(strict_types = 1);
/**
 * Created by PhpStorm.
 * User: sophie.beaupuis
 * Date: 03/01/2017
 * Time: 10:08
 */

namespace Sophpie\Workbench\Plugin\PersistencePlugin\Repository;


use Sophpie\Workbench\DependencyInjection\ContainerNode;
use Sophpie\Workbench\Plugin\PersistencePlugin\RepositoryManager;

abstract class PersistentObjectAbstract implements PersistentObjectInterface
{
    /**
     * @var string
     */
    static protected $objectClassName;

    /**
     * @var Callable
     */
    protected $objectFactory;

    /**
     * @var mixed
     */
    protected $internalObject;

    /**
     * @var $repositoryManager
     */
    protected $repositoryManager;

    /**
     * PersistentObjectAbstract constructor.
     * @param $internalObject
     * @param RepositoryManager $repositoryManager
     */
    public function __construct($internalObject,RepositoryManager $repositoryManager)
    {
        $this->internalObject = $internalObject;
        $this->repositoryManager = $repositoryManager;
    }

    /**
     * @return mixed
     */
    public function getRepositoryManager():RepositoryManager
    {
        return $this->repositoryManager;
    }

    /**
     * @return mixed
     */
    public function getInternalObject()
    {
        return $this->internalObject;
    }

    /**
     * @param mixed $internalObject
     */
    public function setInternalObject($internalObject)
    {
        $this->internalObject = $internalObject;
    }

    /**
     * @return Callable
     */
    public function getObjectFactory(): Callable
    {
        if ( ! $this->objectFactory) {
            return [$this,'objectFactory'];
        }
        return $this->objectFactory;
    }

    /**
     * @param Callable $objectFactory
     */
    public function setObjectFactory(Callable $objectFactory)
    {
        $this->objectFactory = $objectFactory;
    }

    /**
     * @param array $data
     */
    public function objectFactory(array $data)
    {
        $objectClassName = static::$objectClassName;
        $this->internalObject = new $objectClassName();
    }

    /**
     * @param array $data
     */
    public function buildInternalObject(array $data)
    {
        $factory = $this->getObjectFactory();
        call_user_func_array($factory,[$data]);
    }

    /**
     * @param $data
     */
    public function containerNodeBuilder(&$data)
    {
        if ($data instanceof PersistentContainerNode) {
            $data = $data->toArray();
            return;
        }
        if (is_object($data) && get_class($data) == 'stdClass') {
            $arrayObject = new \ArrayObject($data);
            $data = $arrayObject->getIterator()->getArrayCopy();
            array_walk_recursive($data,[$this,'containerNodeBuilder']);
        }
    }

    /**
     * @param $data
     */
    public function persistentContainerNodeBuilder(&$data)
    {
        if (is_array($data)) {
            if (isset($data['type'])) {
                $data = new PersistentContainerNode(ContainerNode::factory($data),$this->getRepositoryManager());
                return;
            }
            array_walk($data,[$this,'persistentContainerNodeBuilder']);
        }
    }

    /**
     * @inheritDoc
     */
    public function bsonSerialize()
    {
        $data = $this->getInternalObject()->jsonSerialize();
        array_walk($data,[$this,'persistentContainerNodeBuilder']);
        return $data;
    }

    /**
     * @inheritDoc
     */
    public function bsonUnserialize(array $data)
    {
        $this->buildInternalObject($data);
        array_walk_recursive($data,[$this, 'containerNodeBuilder']);
        $this->internalObject->configureFromArray($data);
    }
}