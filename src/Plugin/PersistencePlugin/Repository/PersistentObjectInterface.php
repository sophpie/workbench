<?php
declare(strict_types = 1);
/**
 * Created by PhpStorm.
 * User: sophie.beaupuis
 * Date: 03/01/2017
 * Time: 10:07
 */

namespace Sophpie\Workbench\Plugin\PersistencePlugin\Repository;


use MongoDB\BSON\Persistable;

interface PersistentObjectInterface extends Persistable
{
    /**
     * @param mixed $object
     * @return mixed
     */
    public function setInternalObject($object);

    /**
     * @return mixed
     */
    public function getInternalObject();
}