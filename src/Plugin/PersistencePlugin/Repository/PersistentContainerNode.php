<?php
declare(strict_types = 1);
/**
 * Created by PhpStorm.
 * User: sophie.beaupuis
 * Date: 03/01/2017
 * Time: 09:28
 */

namespace Sophpie\Workbench\Plugin\PersistencePlugin\Repository;


use Sophpie\Workbench\DependencyInjection\ContainerNode;

class PersistentContainerNode extends PersistentObjectAbstract
{
    /**
     * @inheritDoc
     */
    public function bsonSerialize()
    {
        $value = $this->getInternalObject()->JsonSerialize();
        return $value;
    }

    /**
     * @inheritDoc
     */
    public function bsonUnserialize(array $data)
    {
        $data['data'] = new \ArrayObject($data['data']);
        $data['data'] = $data['data']->getIterator()->getArrayCopy();
        $this->setInternalObject(new ContainerNode($data));
    }

    /**
     * @return array
     */
    public function toArray():array
    {
        $containerNode = $this->getInternalObject();
        if ($containerNode->getType() == ContainerNode::TYPE_CONSTANT) {
            //@todo : throw exception
        }
        $arrayData = $this->getInternalObject()->jsonSerialize();
        unset($arrayData['__pclass']);
        return $arrayData;
    }


}