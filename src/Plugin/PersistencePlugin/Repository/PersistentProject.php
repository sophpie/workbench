<?php
declare(strict_types = 1);
/**
 * Created by PhpStorm.
 * User: sophie.beaupuis
 * Date: 02/01/2017
 * Time: 16:07
 */

namespace Sophpie\Workbench\Plugin\PersistencePlugin\Repository;

use Sophpie\Workbench\Project\Project;
use Sophpie\Workbench\Test\Context\ContextOutline;

class PersistentProject extends PersistentObjectAbstract
{
    static protected $objectClassName = Project::class;

    /**
     * @inheritDoc
     */
    public function bsonSerialize()
    {
        $data = $this->getInternalObject()->jsonSerialize();
        foreach ($data['contexts'] as &$contextData) {
            $contextOutLine = new ContextOutline();
            $contextOutlineRepository = $this->getRepositoryManager()->getRepository('repository.contextOutlineRepository');
            $contextOutLineId = $contextOutlineRepository->insert($contextOutLine);
            $contextData = $contextOutLineId;
        }
        $data = array_replace(parent::bsonSerialize(),$data);
        return $data;
    }


}