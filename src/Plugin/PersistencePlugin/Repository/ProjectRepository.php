<?php
declare(strict_types = 1);
/**
 * Created by PhpStorm.
 * User: sophie.beaupuis
 * Date: 27/12/2016
 * Time: 13:50
 */

namespace Sophpie\Workbench\Plugin\PersistencePlugin\Repository;


use MongoDB\BSON\ObjectID;
use MongoDB\Driver\BulkWrite;
use MongoDB\Driver\Query;
use Sophpie\Workbench\Project\Project;

class ProjectRepository extends AbstractRepository implements RepositoryInterface
{
    const COLLECTION = 'workbench.project';

    /**
     * @param string $name
     */
    public function get(string $id)
    {
        $query = new Query(['_id' => new ObjectID($id)],['projection' => ['_id '=> 0]]);
        $cursor = $this->getMongoDbManager()->executeQuery(self::COLLECTION,$query);
        foreach ($cursor as $document) {
            $project = $document->getInternalObject();
            return $project;
        }
    }

    /**
     * @param Project $project
     */
    public function insert(Project $project)
    {
        $bulk = new BulkWrite();
        //@todo : check if name already in use
        $persistedProject = new PersistentProject($project,$this->getRepositoryManager());
        $bulk->insert($persistedProject);
        $this->getMongoDbManager()->executeBulkWrite(self::COLLECTION,$bulk);
    }
}