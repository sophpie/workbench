<?php
declare(strict_types = 1);
/**
 * Created by PhpStorm.
 * User: sophie.beaupuis
 * Date: 03/01/2017
 * Time: 10:36
 */

namespace Sophpie\Workbench\Plugin\PersistencePlugin\Repository;


use MongoDB\BSON\ObjectID;
use MongoDB\Driver\BulkWrite;
use Sophpie\Workbench\Test\Context\ContextOutline;

class ContextOutlineRepository extends AbstractRepository implements RepositoryInterface
{
    const COLLECTION = "workbench.context";

    /**
     * @param ContextOutline $contextOutline
     */
    public function insert(ContextOutline $contextOutline):ObjectID
    {
        $bulk = new BulkWrite();
        //@todo : check if name already in use
        $persistedProject = new PersistentContextOutline($contextOutline,$this->getRepositoryManager());
        $id = $bulk->insert($persistedProject);
        $this->getMongoDbManager()->executeBulkWrite(self::COLLECTION,$bulk);
        return $id;
    }
}