<?php
declare(strict_types = 1);
/**
 * Created by PhpStorm.
 * User: sophie.beaupuis
 * Date: 03/01/2017
 * Time: 10:15
 */

namespace Sophpie\Workbench\Plugin\PersistencePlugin\Repository;


use Sophpie\Workbench\Test\Context\ContextOutline;

class PersistentContextOutline extends PersistentObjectAbstract
{
    static protected $objectClassName = ContextOutline::class;
}