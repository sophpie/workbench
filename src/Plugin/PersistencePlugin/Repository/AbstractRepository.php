<?php
declare(strict_types = 1);
/**
 * Created by PhpStorm.
 * User: sophie.beaupuis
 * Date: 27/12/2016
 * Time: 10:54
 */

namespace Sophpie\Workbench\Plugin\PersistencePlugin\Repository;


use MongoDB\BSON\ObjectID;
use MongoDB\Driver\Manager;
use MongoDB\Driver\Query;
use Sophpie\Workbench\Plugin\PersistencePlugin\RepositoryManager;

abstract class AbstractRepository
{
    const COLLECTION = '';

    /**
     * @var Manager
     */
    protected $mongoDbManager;

    /**
     * @var RepositoryManager
     */
    protected $repositoryManager;

    /**
     * AbstractRepository constructor.
     * @param Manager $mongoDbManager
     * @param RepositoryManager $repositoryManager
     */
    public function __construct(Manager $mongoDbManager, RepositoryManager $repositoryManager)
    {
        $this->mongoDbManager = $mongoDbManager;
        $this->repositoryManager = $repositoryManager;
    }

    /**
     * @return Manager
     */
    public function getMongoDbManager(): Manager
    {
        return $this->mongoDbManager;
    }

    /**
     * @param Manager $mongoDbManager
     */
    public function setMongoDbManager(Manager $mongoDbManager)
    {
        $this->mongoDbManager = $mongoDbManager;
    }

    /**
     * @return RepositoryManager
     */
    public function getRepositoryManager(): RepositoryManager
    {
        return $this->repositoryManager;
    }

    /**
     * @param string $name
     */
    public function get(string $id)
    {
        $query = new Query(['_id' => new ObjectID($id)],['projection' => ['_id '=> 0]]);
        $cursor = $this->getMongoDbManager()->executeQuery(static::COLLECTION,$query);
        foreach ($cursor as $document) {
            return $document->getInternalObject();
        }
    }
}