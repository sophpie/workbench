<?php
declare(strict_types = 1);
/**
 * Created by PhpStorm.
 * User: sophie.beaupuis
 * Date: 03/01/2017
 * Time: 14:26
 */

namespace Sophpie\Workbench\Plugin\PersistencePlugin;


use Sophpie\Workbench\Plugin\PersistencePlugin\Repository\RepositoryInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

class RepositoryManager implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * RepositoryManager constructor.
     */
    public function __construct(ContainerInterface $container)
    {
        $this->setContainer($container);
    }

    /**
     * @param $repositoryAlias
     * @return RepositoryInterface
     */
    public function getRepository($repositoryAlias):RepositoryInterface
    {
        return $this->container->get($repositoryAlias);
    }
}