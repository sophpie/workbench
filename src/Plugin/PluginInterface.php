<?php
/**
 * Created by PhpStorm.
 * User: sophie.beaupuis
 * Date: 02/01/2017
 * Time: 09:54
 */

namespace Sophpie\Workbench\Plugin;


use Symfony\Component\EventDispatcher\EventSubscriberInterface;

interface PluginInterface extends EventSubscriberInterface
{
    /**
     * ExtensionInterface constructor.
     * @param array $options
     */
    public function __construct(array $options);

    /**
     * @return string
     */
    public function getName():string;
}