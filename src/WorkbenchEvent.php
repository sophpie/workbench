<?php
declare(strict_types = 1);
/**
 * Created by PhpStorm.
 * User: sophie.beaupuis
 * Date: 29/12/2016
 * Time: 08:24
 */

namespace Sophpie\Workbench;


use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\EventDispatcher\Event;

/**
 * Class WorkbenchEvent
 * @package Sophpie\Workbench
 */
class WorkbenchEvent extends Event
{
    /**
     * @var ContainerBuilder
     */
    protected $containerBuilder;

    /**
     * @return ContainerBuilder
     */
    public function getContainerBuilder(): ContainerBuilder
    {
        return $this->containerBuilder;
    }

    /**
     * @param ContainerBuilder $containerBuilder
     */
    public function setContainerBuilder(ContainerBuilder $containerBuilder)
    {
        $this->containerBuilder = $containerBuilder;
    }


}