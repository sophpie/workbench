<?php
/**
 * Created by PhpStorm.
 * User: sophie.beaupuis
 * Date: 03/01/2017
 * Time: 15:39
 */

namespace Sophpie\Workbench\Test\Generator;


use Sophpie\Workbench\Test\TestSuite\TestSuite;

interface GeneratorInterface
{
    /**
     * @return TestSuite
     */
    public function getTestSuite():TestSuite;
}