<?php
declare(strict_types = 1);
/**
 * Created by PhpStorm.
 * User: sophie.beaupuis
 * Date: 21/12/2016
 * Time: 09:05
 */

namespace Sophpie\Workbench\Test\Sample;

use Symfony\Component\DependencyInjection\Definition;

/**
 * Class DefinitionWrapper
 *
 * Wrap Definition object to be export as Json
 * @package Sophpie\Workbench\Test\Sample
 */
class DefinitionWrapper implements \JsonSerializable
{
    /**
     * @var Definition
     */
    protected $definition;

    /**
     * @param Definition $definition
     */
    public function setDefinition(Definition $definition)
    {
        $this->definition = $definition;
    }

    /**
     * @inheritDoc
     */
    public function jsonSerialize()
    {
        $jsonArray = [
            "class" => $this->definition->getClass()
        ];

        return $jsonArray;
    }

    public static function fromArray(array $data):Definition
    {
        $definition = new Definition();
        $definition->setClass($data['class']);
        return $definition;
    }


}