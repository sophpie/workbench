<?php
declare(strict_types = 1);
/**
 * Created by PhpStorm.
 * User: sophie.beaupuis
 * Date: 19/12/2016
 * Time: 10:07
 */

namespace Sophpie\Workbench\Test\Sample;


use Sophpie\Workbench\Probe\ProbeInterface;

class Sample
{
    /**
     * @var array
     */
    protected $probes = [];

    /**
     * @var string
     */
    protected $functionName;

    /**
     * @return string
     */
    public function getFunctionName(): string
    {
        return $this->functionName;
    }

    /**
     * @param string $functionName
     */
    public function setFunctionName(string $functionName)
    {
        $this->functionName = $functionName;
    }

    /**
     * @return array
     */
    public function getProbes(): array
    {
        return $this->probes;
    }

    /**
     * @param $probe
     */
    public function addProbe(ProbeInterface $probe)
    {
        $this->probes[get_class($probe)] = $probe;
    }

    /**
     * @param string $probeClass
     * @return bool
     */
    public function hasProbe(string $probeClass):bool
    {
        return isset($this->probes[$probeClass]);
    }
}