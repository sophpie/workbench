<?php
declare(strict_types = 1);
/**
 * Created by PhpStorm.
 * User: sophie.beaupuis
 * Date: 20/12/2016
 * Time: 08:44
 */

namespace Sophpie\Workbench\Test\Sample;


use Sophpie\Workbench\DependencyInjection\ContainerNode;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Reference;

class SampleBuilder
{
    /**
     * @var ContainerBuilder
     */
    protected $containerBuilder;

    /**
     * @var Definition
     */
    protected $sampleDefinition;

    /**
     * SampleBuilder constructor.
     * @param ContainerBuilder $containerBuilder
     */
    public function __construct(ContainerBuilder $containerBuilder)
    {
        $this->containerBuilder = $containerBuilder;
    }

    /**
     * @param SampleOutline $outline
     */
    public function build(SampleOutline $outline)
    {
        $this->sampleDefinition = new Definition(Sample::class);
        $this->sampleDefinition->addMethodCall('setFunctionName',[$outline->getFunctionName()]);
        $this->configureProbes($outline);
        $this->containerBuilder->addDefinitions([
            'sample' => $this->sampleDefinition,
        ]);
    }

    /**
     * @param SampleOutline $outline
     */
    protected function configureProbes(SampleOutline $outline)
    {
        //@todo : check if a probe of same class has already been added to sample
        foreach ($outline->getProbes() as $probe) {
            /** @var ContainerNode $probe */
            if ($probe->getType() == ContainerNode::TYPE_DEFINITION) {
                $probeDefinition = $probe->getInternalNode();
                $serviceId = $probeDefinition->getClass();
                if ( ! $this->containerBuilder->hasDefinition($serviceId)) {
                    $this->containerBuilder->addDefinitions([$serviceId => $probeDefinition]);
                }
                else {
                    $probeDefinition = new Reference($serviceId);
                }
                $probeDefinition->addTag('workbench.subscriber');
                $this->sampleDefinition->addMethodCall('addProbe',[$probeDefinition]);
            }
            if ($probe->getType() == ContainerNode::TYPE_REFERENCE) {
                $probeDefinition = $probe->getDefinition();
                if ( ! $probeDefinition->hasTag('workbench.subscriber')) {
                    $probeDefinition->addTag('workbench.subscriber');
                }
                $this->sampleDefinition->addMethodCall('addProbe',[$probe->getInternalNode()]);
            }

        }
    }
}