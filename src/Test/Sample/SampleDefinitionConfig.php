<?php
declare(strict_types = 1);
/**
 * Created by PhpStorm.
 * User: sophie.beaupuis
 * Date: 23/12/2016
 * Time: 11:38
 */

namespace Sophpie\Workbench\Test\Sample;


use Sophpie\Workbench\Api\WorkbenchTreeBuilder;
use Sophpie\Workbench\Probe\ProbeServiceConfig;
use Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class SampleDefinitionConfig implements ConfigurationInterface
{
    /**
     * @inheritDoc
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new WorkbenchTreeBuilder();
        $root = $treeBuilder->root('sample','array');
        $this->probesConfiguration($root);
    }

    protected function probesConfiguration(ArrayNodeDefinition $node)
    {
        $probeServiceConfig = new ProbeServiceConfig();
        $probesNode = $node->children()->ArrayNode('probes');
        foreach ($probeServiceConfig->getConfigTreeBuilder()->getRoot()->children() as $probeNode) {
            $probesNode->addChild($probeNode);
        }
    }

}