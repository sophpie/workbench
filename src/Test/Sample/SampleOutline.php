<?php
declare(strict_types = 1);
/**
 * Created by PhpStorm.
 * User: sophie.beaupuis
 * Date: 20/12/2016
 * Time: 08:41
 */

namespace Sophpie\Workbench\Test\Sample;


use Sophpie\Workbench\DependencyInjection\ContainerNode;
use Sophpie\Workbench\Test\Outline\OutlineAbstract;
use Sophpie\Workbench\Project\Project;

/**
 * Class SampleDefinition
 *
 * Object that will be used by SampleBuilder to configure Sample as a bunch of services within
 * workbench container
 *
 * @package Sophpie\Workbench\Test\Sample
 */
class SampleOutline extends OutlineAbstract
{
    /**
     * @var array
     */
    protected $probes = [];

    /**
     * @var string
     */
    protected $functionName = '';

    /**
     * @return string
     */
    public function getFunctionName()
    {
        return $this->functionName;
    }

    /**
     * @param string $functionName
     */
    public function setFunctionName(string $functionName)
    {
        $this->functionName = $functionName;
    }

    /**
     * @return array
     */
    public function getProbes(): array
    {
        return $this->probes;
    }

    /**
     * @param ContainerNode $probe
     */
    public function addProbe(ContainerNode $probe)
    {
        $this->probes[$probe->getId()] = $probe;
    }

    /**
     * @param Project $project
     */
    public function mergeProjectProbes(Project $project)
    {
        foreach ($project->getProbes() as $probe) {
            $this->addProbe($probe);
        }
    }

    /**
     * @inheritDoc
     */
    public function toArray():array
    {
        $jsonArray = [];
        if ( ! empty($this->functionName)){
            $jsonArray['functionName'] = $this->getFunctionName();
        }
        if (count($this->probes) > 0) {
            $jsonArray['probes'] = [];
            foreach ($this->getProbes() as $probeNode) {
                $jsonArray['probes'] = $probeNode->jsonSerialize();
            }
        }
        return $jsonArray;
    }

    /**
     * @param array $data
     * @param Project|null $project
     * @return SampleOutline
     */
    public static function fromArray(array $data)
    {
        $config = new self();
        $config->configureFromArray($data);
        return $config;
    }

    /**
     * @param array $data
     */
    public function configureFromArray(array $data)
    {
        if (isset($data['probes'])) {
            foreach ($data['probes'] as $probe) {
                $this->addProbe(ContainerNode::factory($probe));
            }
        }
        if (isset($data['functionName'])) {
            $this->setFunctionName($data['functionName']);
        }
    }
}