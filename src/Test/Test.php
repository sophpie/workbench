<?php
declare(strict_types = 1);
/**
 * Created by PhpStorm.
 * User: sophie.beaupuis
 * Date: 28/12/2016
 * Time: 09:19
 */

namespace Sophpie\Workbench\Test;


use Sophpie\Workbench\Project\Project;
use Sophpie\Workbench\Test\Context\Context;
use Sophpie\Workbench\Test\Sample\Sample;

/**
 * Class Test
 * @package Sophpie\Workbench\Test
 */
class Test implements \JsonSerializable
{
    /**
     * @var Project
     */
    protected $project;

    /**
     * @var Sample
     */
    protected $sample;

    /**
     * @var Context
     */
    protected $context;

    /**
     * @return Project
     */
    public function getProject(): Project
    {
        return $this->project;
    }

    /**
     * @param Project $project
     */
    public function setProject(Project $project)
    {

        $this->project = $project;
    }

    /**
     * @return bool
     */
    public function hasProject():bool
    {
        return ! is_null($this->project);
    }

    /**
     * @return Sample
     */
    public function getSample(): Sample
    {
        return $this->sample;
    }

    /**
     * @param Sample $sample
     */
    public function setSample(Sample $sample)
    {
        $this->sample = $sample;
    }

    /**
     * @return Context
     */
    public function getContext(): Context
    {
        return $this->context;
    }

    /**
     * @param Context $context
     */
    public function setContext(Context $context)
    {
        $this->context = $context;
    }

    /**
     * @inheritDoc
     */
    public function jsonSerialize()
    {
        $jsonArray = [];
        if ($this->hasProject()) {
            $jsonArray['project'] = $this->getProject()->jsonSerialize();
        }
        $jsonArray['context'] = $this->getContext()->jsonSerialize();
        return $jsonArray;
    }

    public function configureFromArray(array $data)
    {
        $context = new Context();
        $this->setContext($data['context']);
    }


}