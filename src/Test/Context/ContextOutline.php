<?php
declare(strict_types = 1);
/**
 * Created by PhpStorm.
 * User: sophie.beaupuis
 * Date: 21/12/2016
 * Time: 14:19
 */

namespace Sophpie\Workbench\Test\Context;

use Sophpie\Workbench\Assertion\AssertionTest;
use Sophpie\Workbench\DependencyInjection\ContainerNode;
use Sophpie\Workbench\Test\Outline\OutlineAbstract;


/**
 * Class ContextOutline
 * @package Sophpie\Workbench\Test\Context
 */
class ContextOutline extends OutlineAbstract
{
    const DEPENDENCIES = 'dependencies';

    /**
     * @var array
     */
    protected $arguments = [];

    /**
     * @var array
     */
    protected $assertionTests = [];

    /**
     * @var ContainerNode
     */
    protected $instance;

    /**
     * @var string
     */
    protected $id;

    /**
     * @var array
     */
    protected $dependencies = [];

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId(string $id)
    {
        $this->id = $id;
    }

    /**
     * @return bool
     */
    public function hasId():bool
    {
        return  ! empty($this->id);
    }

    /**
     * @return ContainerNode
     */
    public function getInstance()
    {
        return $this->instance;
    }

    /**
     * @param ContainerNode $instance
     */
    public function setInstance($instance)
    {
        $this->instance = $instance;
    }

    /**
     * @return array
     */
    public function getArguments(): array
    {
        return $this->arguments;
    }

    /**
     * @param ContainerNode $argument
     * @param int|null $position
     */
    public function addArgument($argument,int $position = null)
    {
        if ($position) $this->arguments[$position] = $argument;
        else $this->arguments[] = $argument;
    }

    /**
     * @param AssertionTest $assertionTest
     */
    public function addAssertionTest(AssertionTest $assertionTest)
    {
        $this->assertionTests[] = $assertionTest;
    }

    /**
     * @return array
     */
    public function getAssertionTests(): array
    {
        return $this->assertionTests;
    }

    /**
     * @return bool
     */
    public function hasInstance():bool
    {
        return  ! is_null($this->instance);
    }

    /**
     * @return array
     */
    public function getDependencies(): array
    {
        return $this->dependencies;
    }

    /**
     * @param array $data
     * @return ContextOutline
     */
    public static function fromArray(array $data):ContextOutline
    {
        $definition = new self();
        $definition->configureFromArray($data);
        return $definition;
    }

    /**
     * @param array $data
     */
    public function configureFromArray(array $data)
    {
        if (isset($data['id'])) {
            $this->setId($data['id']);
        }
        if (isset($data['arguments'])) {
            foreach ($data['arguments'] as $argument) {
                $argument = ContainerNode::factory($argument);
                $this->addArgument($argument);
            }
        }
        if (isset($data['assertionTests'])) {
            foreach ($data['assertionTests'] as $testData) {
                $assertion = ContainerNode::factory($testData['assertion']);
                $expectedValue = ContainerNode::factory($testData['expectedValue']);
                $assertionTest = new AssertionTest($assertion,$testData['dataPath'],$expectedValue);
                $this->addAssertionTest($assertionTest);
            }
        }
        if (isset($data['instance'])) {
            $instance = ContainerNode::factory($data['instance']);
            $this->setInstance($instance);
        }
        if (isset($data[self::DEPENDENCIES])) {
            foreach ($data[self::DEPENDENCIES] as $id => $dependency) {
                $containerNode = new ContainerNode($dependency);
                $this->dependencies[$id] = $containerNode;
            }
        }
    }

    /**
     * @inheritDoc
     */
    public function toArray():array
    {
        $jsonArray = [];
        if ($this->hasId()) {
            $jsonArray['id'] = $this->getId();
        }
        if ($this->hasInstance()) {
            $jsonArray['instance'] = $this->getInstance()->jsonSerialize();
        }
        $jsonArray['assertionTests'] = [];
        foreach ($this->assertionTests as $assertionTest) {
            $jsonArray['assertionTests'][] = [
                'assertion' => $assertionTest->getAssertion()->jsonSerialize(),
                'dataPath' => $assertionTest->getRealValueDataPath(),
                'expectedValue' => $assertionTest->getExpectedValue()->jsonSerialize(),
            ];
        }
        $jsonArray['arguments'] = [];
        foreach ($this->arguments as $pos => $argument) {
            $jsonArray['arguments'][(int)$pos] = $argument->jsonSerialize();
        }
        if (count($this->dependencies) > 0) {
            $jsonArray[self::DEPENDENCIES] = [];
            foreach ($this->dependencies as $id => $containerNode) {
                $jsonArray[self::DEPENDENCIES][$id] = $containerNode->jsonSerialize();
            }
        }
        return $jsonArray;
    }

}