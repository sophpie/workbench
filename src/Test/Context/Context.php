<?php
declare(strict_types = 1);
/**
 * Created by PhpStorm.
 * User: sophie.beaupuis
 * Date: 20/12/2016
 * Time: 09:46
 */

namespace Sophpie\Workbench\Test\Context;


use Sophpie\Workbench\Assertion\AssertionTest;
use Sophpie\Workbench\Project\Project;

/**
 * Class Context
 * @package Sophpie\Workbench\Test\Context
 */
class Context implements \JsonSerializable
{
    /**
     * @var array
     */
    protected $arguments = [];

    /**
     * @var array
     */
    protected $assertionTests = [];

    /**
     * @var mixed|null
     */
    protected $instance;

    /**
     * @var Project|null
     */
    protected $project;

    /**
     * @return mixed|null
     */
    public function getInstance()
    {
        return $this->instance;
    }

    /**
     * @param mixed|null $instance
     */
    public function setInstance($instance)
    {
        $this->instance = $instance;
    }

    /**
     * @return bool
     */
    public function hasInstance():bool
    {
        return ! is_null($this->instance);
    }

    /**
     * @return array
     */
    public function getArguments(): array
    {
        return $this->arguments;
    }

    /**
     * @param array $arguments
     */
    public function setArguments(array $arguments)
    {
        $this->arguments = $arguments;
    }

    /**
     * @return array
     */
    public function getAssertionTests(): array
    {
        return $this->assertionTests;
    }

    /**
     * @param AssertionTest $assertionTest
     */
    public function addAssertionTest(AssertionTest $assertionTest)
    {
        $this->assertionTests[] = $assertionTest;
    }

    /**
     * @return Project
     */
    public function getProject():Project
    {
        return $this->project;
    }

    /**
     * @param Project $project
     */
    public function setProject(Project $project)
    {
        $this->project = $project;
    }

    /**
     * @return bool
     */
    public function hasProject():bool
    {
        return ! is_null($this->project);
    }

    /**
     * @inheritDoc
     */
    public function jsonSerialize()
    {
        $array = [];
        if ($this->hasProject()) {
            $array['project'] = $this->getProject()->toArray();
        }
        $array['instance'] = get_class($this->getInstance());
        if (count($this->getArguments()) > 0) {
            $array['arguments'] = [];
            foreach ($this->getArguments() as $argument) {
                $array['arguments'][] = $argument;
            }
        }
        if (count($this->getAssertionTests()) > 0) {
            $array['assertionTests'] = [];
            foreach ($this->getAssertionTests() as $assertionTest) {
                $array['assertionTests'][] = $assertionTest;
            }
        }
        return $array;
    }
}