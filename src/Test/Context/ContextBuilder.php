<?php
declare(strict_types = 1);
/**
 * Created by PhpStorm.
 * User: sophie.beaupuis
 * Date: 21/12/2016
 * Time: 14:45
 */

namespace Sophpie\Workbench\Test\Context;


use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Class ContextBuilder
 * @package Sophpie\Workbench\Test\Context
 */
class ContextBuilder
{
    /**
     * @var ContainerBuilder
     */
    protected $containerBuilder;

    /**
     * @var Definition
     */
    protected $contextDefinition;

    /**
     * ContextBuilder constructor.
     * @param ContainerBuilder $containerBuilder
     */
    public function __construct(ContainerBuilder $containerBuilder)
    {
        $this->containerBuilder = $containerBuilder;
    }

    /**
     * @param ContextOutline $outline
     */
    public function build(ContextOutline $outline)
    {
        $this->contextDefinition = new Definition(Context::class);
        $this->configureDependencies($outline);
        $this->configureCallableArguments($outline);
        $this->configureAssertionTests($outline);
        $this->configureInstance($outline);
        $this->containerBuilder->addDefinitions([
            'context' => $this->contextDefinition,
        ]);
    }

    /**
     * @param ContextOutline $outline
     */
    protected function configureInstance(ContextOutline $outline)
    {
        if ( ! $outline->hasInstance()) return;
        $this->contextDefinition->addMethodCall('setInstance',[$outline->getInstance()->getInternalNode()]);
    }

    /**
     * @param ContextOutline $outline
     */
    protected function configureCallableArguments(ContextOutline $outline)
    {
        $arguments = [];
        foreach ($outline->getArguments() as $argumentNode) {
            $arguments[] = $argumentNode->getInternalNode();
        }
        $this->contextDefinition->addMethodCall('setArguments',[$arguments]);
    }


    /**
     * @param ContextOutline $outline
     */
    protected function configureAssertionTests(ContextOutline $outline)
    {
        foreach ($outline->getAssertionTests() as $assertionTest) {
            $assertion = $assertionTest->getAssertion()->getInternalNode();
            if ($assertion instanceof Definition) {
                $serviceId = $assertion->getClass();
                if ($this->containerBuilder->hasDefinition($serviceId)) {
                    $assertionTest->setAssertion(new Reference($serviceId));
                }
                else {
                    $this->containerBuilder->addDefinitions([$serviceId => $assertion]);
                }
            }
            $this->contextDefinition->addMethodCall('addAssertionTest',[$assertionTest]);
        }
    }

    /**
     * @param ContextOutline $outline
     */
    protected function configureDependencies(ContextOutline $outline)
    {
        foreach ($outline->getDependencies() as $id => $node) {
            $dependency = $node->getInternaleNode();
            if ($dependency instanceof Definition) {
                $this->containerBuilder->addDefinitions([$id,$dependency]);
            }
        }
    }

}