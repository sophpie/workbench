<?php
declare(strict_types = 1);

/**
 * Created by PhpStorm.
 * User: sophie.beaupuis
 * Date: 20/12/2016
 * Time: 14:30
 */

namespace Sophpie\Workbench\Test\DataMiner;

use Sophpie\Workbench\Test\TestEvent;

/**
 * Class DataMiner
 * @package Sophpie\Workbench\Test\DataMiner
 */
class DataMiner
{
    const DOMAIN_RESULT = "result";
    /**
     * @var TestEvent
     */
    protected $testEvent;

    /**
     * @param TestEvent $testEvent
     */
    public function setTestEvent(TestEvent $testEvent)
    {
        $this->testEvent = $testEvent;
    }

    /**
     * @param string $domain
     * @param string $dataPath
     * @return mixed|null
     */
    public function findData(string $dataPath)
    {
        $dataPathArray = explode('.',$dataPath);
        $domain = array_shift($dataPathArray);
        $sourceDomain = $this->getDomainData($domain);
        if (count($dataPathArray) == 0) return $sourceDomain;
        return $this->inceptionLookUp($sourceDomain,$dataPathArray);
    }

    /**
     * @param $dataSource
     * @param array $dataPath
     * @return mixed|null
     */
    protected function inceptionLookUp($dataSource,array $dataPath)
    {
        $offset = array_shift($dataPath);
        $result = null;
        if (is_array($dataSource)) $result = $dataSource[$offset];
        if ($dataSource instanceof \ArrayAccess) $result = $dataSource->offsetGet($offset);
        //..
        if (count($dataPath) == 0) return $result;
        return $this->inceptionLookUp($result,$dataPath);
    }

    /**
     * @param $domain
     * @return mixed
     */
    public function getDomainData($domain)
    {
        if ($domain == self::DOMAIN_RESULT) return $this->testEvent->getResult()->getExecutionResult();
    }

}