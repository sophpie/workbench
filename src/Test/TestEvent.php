<?php
declare(strict_types = 1);
/**
 * Created by PhpStorm.
 * User: sophie.beaupuis
 * Date: 19/12/2016
 * Time: 10:27
 */

namespace Sophpie\Workbench\Test;

use Sophpie\Workbench\Test\Result\Result;
use Symfony\Component\EventDispatcher\Event;

/**
 * Class TestEvent
 * @package Sophpie\Workbench\Test
 */
class TestEvent extends Event
{
    /**
     * @var Result;
     */
    protected $result;

    /**
     * @var array
     */
    protected $callableArguments = [];

    /**
     * @var bool
     */
    protected $isSuccess = true;

    /**
     * @var mixed
     */
    protected $callable;

    /**
     * @var Test
     */
    protected $test;

    /**
     * @return Test
     */
    public function getTest(): Test
    {
        return $this->test;
    }

    /**
     * @param Test $test
     */
    public function setTest(Test $test)
    {
        $this->test = $test;
    }

    /**
     * @return mixed
     */
    public function getCallable()
    {
        return $this->callable;
    }

    /**
     * @param mixed $callable
     */
    public function setCallable($callable)
    {
        $this->callable = $callable;
    }

    /**
     * @return array
     */
    public function getCallableArguments(): array
    {
        return $this->callableArguments;
    }

    /**
     * @param array $callableArguments
     */
    public function setCallableArguments(array $callableArguments)
    {
        $this->callableArguments = $callableArguments;
    }

    /**
     * @return Result
     */
    public function getResult(): Result
    {
        if ( ! $this->result) {
            $this->result = new Result();
        }
        return $this->result;
    }

    /**
     * @param Result $result
     */
    public function setResult(Result $result)
    {
        $this->result = $result;
    }

    /**
     * @return bool
     */
    public function isSuccess(): bool
    {
        return $this->isSuccess;
    }

    /**
     * @param bool $isSuccess
     */
    public function setSuccess(bool $isSuccess)
    {
        $this->isSuccess = $isSuccess;
    }



}