<?php
declare(strict_types = 1);
/**
 * Created by PhpStorm.
 * User: sophie.beaupuis
 * Date: 19/12/2016
 * Time: 10:05
 */

namespace Sophpie\Workbench\Test;


use Sophpie\Workbench\Test\Result\Result;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Class TestManager
 * @package Sophpie\Workbench\Test
 */
class TestManager
{
    const EVENT_RUN = 'test.run';
    const EVENT_PREPARE = 'test.prepare';
    const EVENT_POST_RUN = 'test.post.run';
    const EVENT_RESULT = 'test.result';

    /**
     * @var TestEvent
     */
    protected $testEvent;

    /**
     * @var EventDispatcherInterface
     */
    protected $dispatcher;

    /**
     * TestManager constructor.
     * @param EventDispatcherInterface $dispatcher
     */
    public function __construct(EventDispatcherInterface $dispatcher)
    {
        $this->dispatcher = $dispatcher;
    }

    /**
     * @param Test $test
     * @return Result
     */
    public function runTest(Test $test)
    {
        $this->testEvent = new TestEvent();
        $this->testEvent->setTest($test);
        $this->dispatcher->dispatch(self::EVENT_PREPARE,$this->testEvent);
        $this->dispatcher->dispatch(self::EVENT_RUN,$this->testEvent);
        $this->dispatcher->dispatch(self::EVENT_POST_RUN,$this->testEvent);
        $this->dispatcher->dispatch(self::EVENT_RESULT,$this->testEvent);
        $result = $this->testEvent->getResult();
        $result->setTest($test);
        return $result;
    }
}