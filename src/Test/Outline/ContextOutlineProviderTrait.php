<?php
/**
 * Created by PhpStorm.
 * User: sophie.beaupuis
 * Date: 05/01/2017
 * Time: 09:41
 */

namespace Sophpie\Workbench\Test\Outline;


use Sophpie\Workbench\Test\Context\ContextOutline;

/**
 * Class ContextOutlineProviderTrait
 * @package Sophpie\Workbench\Test\Outline
 * @see ContextOutlineProvider
 */
trait ContextOutlineProviderTrait
{
    /**
     * @var ContextOutline
     */
    protected $contextOutline;

    /**
     * @return ContextOutline
     * @see ContextOutlineProvider::getContextOutline()
     */
    public function getContextOutline():ContextOutline
    {
        return $this->contextOutline;
    }

    /**
     * @param ContextOutline $contextOutline
     */
    public function setContextOutline(ContextOutline $contextOutline)
    {
        $this->contextOutline = $contextOutline;
    }
}