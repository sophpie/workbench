<?php
/**
 * Created by PhpStorm.
 * User: sophie.beaupuis
 * Date: 03/01/2017
 * Time: 16:35
 */

namespace Sophpie\Workbench\Test\Outline;


/**
 * Interface OutlineInterface
 * @package Sophpie\Workbench
 */
interface OutlineInterface
{
    /**
     * @param array $data
     * @return mixed
     */
    public function configureFromArray(array $data);

    /**
     * @param OutlineInterface $outline
     * @return mixed
     */
    public function addChild(OutlineInterface $outline);

    /**
     * @return array
     */
    public function getChildren():array;

    /**
     * @param OutlineInterface $outline
     * @return mixed
     */
    public function setParent(OutlineInterface $outline);

    /**
     * @return OutlineInterface
     */
    public function getParent():OutlineInterface;

    /**
     * @return bool
     */
    public function hasChild():bool;

    /**
     * @return bool
     */
    public function hasParent():bool;

    /**
     * @param OutlineInterface $parent
     * @return mixed
     */
    public function appendParent(OutlineInterface $parent);

    /**
     * @param OutlineInterface $a
     * @param OutlineInterface $b
     * @return OutlineInterface
     */
    public static function override(OutlineInterface $a, OutlineInterface $b):OutlineInterface;

    /**
     * @return array
     */
    public function toArray():array;

}