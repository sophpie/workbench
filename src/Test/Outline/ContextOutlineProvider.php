<?php
/**
 * Created by PhpStorm.
 * User: sophie.beaupuis
 * Date: 05/01/2017
 * Time: 08:21
 */

namespace Sophpie\Workbench\Test\Outline;


use Sophpie\Workbench\Test\Context\ContextOutline;

/**
 * Interface ContextOutlineProvider
 * @package Sophpie\Workbench\Test\Outline
 */
interface ContextOutlineProvider
{
    /**
     * @return ContextOutline
     */
    public function getContextOutline():ContextOutline;

    /**
     * @param ContextOutline $contextOutline
     * @return mixed
     */
    public function setContextOutline(ContextOutline $contextOutline);
}