<?php
/**
 * Created by PhpStorm.
 * User: sophie.beaupuis
 * Date: 05/01/2017
 * Time: 09:19
 */

namespace Sophpie\Workbench\Test\Outline;


use Sophpie\Workbench\Project\Project;

/**
 * Interface ProjectAwareInterface
 * @package Sophpie\Workbench\Test\Outline
 */
interface ProjectAwareInterface
{
    /**
     * @return bool
     */
    public function hasProject():bool;

    /**
     * @return Project
     */
    public function getProject():Project;

    /**
     * @param Project $project
     * @return mixed
     */
    public function setProject(Project $project);
}