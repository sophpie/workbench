<?php
/**
 * Created by PhpStorm.
 * User: sophie.beaupuis
 * Date: 05/01/2017
 * Time: 09:22
 */

namespace Sophpie\Workbench\Test\Outline;


use Sophpie\Workbench\Project\Project;

/**
 * Class ProjectAwareTrait
 * @package Sophpie\Workbench\Test\Outline
 */
trait ProjectAwareTrait
{
    /**
     * @var Project
     */
    protected $project;

    /**
     * @return bool
     * @see ProjectAwareInterface::hasProject()
     */
    public function hasProject():bool
    {
        return ! is_null($this->project);
    }

    /**
     * @param Project $project
     * @see ProjectAwareInterface::setProject()
     */
    public function setProject(Project $project)
    {
        $this->project = $project;
    }

    /**
     * @return Project
     * @see ProjectAwareInterface::getProject()
     */
    public function getProject():Project
    {
        return $this->project;
    }
}