<?php
/**
 * Created by PhpStorm.
 * User: sophie.beaupuis
 * Date: 05/01/2017
 * Time: 09:36
 */

namespace Sophpie\Workbench\Test\Outline;


use Sophpie\Workbench\Test\Sample\SampleOutline;

/**
 * Class SampleOutlineProviderTrait
 * @package Sophpie\Workbench\Test\Outline
 * @see SampleOutlineProvider
 */
trait SampleOutlineProviderTrait
{
    /**
     * @var SampleOutline
     */
    protected $sampleOutline;

    /**
     * @return SampleOutline
     * @see SampleOutlineProvider::getSampleOutline()
     */
    public function getSampleOutline():SampleOutline
    {
        return $this->sampleOutline;
    }

    /**
     * @param SampleOutline $sampleOutline
     */
    public function setSampleOutline(SampleOutline $sampleOutline)
    {
        $this->sampleOutline = $sampleOutline;
    }
}