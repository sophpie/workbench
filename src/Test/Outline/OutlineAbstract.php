<?php
declare(strict_types = 1);
/**
 * Created by PhpStorm.
 * User: sophie.beaupuis
 * Date: 03/01/2017
 * Time: 15:52
 */

namespace Sophpie\Workbench\Test\Outline;

use Sophpie\Workbench\Test\Context\ContextOutline;
use Sophpie\Workbench\Test\Sample\Sample;
use Sophpie\Workbench\Test\Sample\SampleOutline;


/**
 * Class OutlineAbstract
 * @package Sophpie\Workbench
 */
abstract class OutlineAbstract implements OutlineInterface
{
    /**
     * @var array
     */
    protected $children = [];

    /**
     * @var OutlineInterface
     */
    protected $parent;

    /**
     * @return array
     */
    public function getChildren(): array
    {
        return $this->children;
    }

    /**
     * @param OutlineAbstract $outLine
     */
    public function addChild(OutlineInterface $outLine)
    {
        $this->children[] = $outLine;
        if ($outLine->getParent() === $this) return
        $outLine->setParent($this);
    }

    /**
     * @return bool
     */
    public function hasChild():bool
    {
        return count($this->children) > 0;
    }

    /**
     * @inheritDoc
     */
    public function setParent(OutlineInterface $outline)
    {
        $this->parent = $outline;
    }

    /**
     * @return \SplStack
     */
    protected function getParentStack():\SplStack
    {
        $parentStack = new \SplStack();
        $parent = $this;
        while($parent->hasParent()) {
            $parent = $parent->getParent();
            $parentStack->push($parent);
        }
        return $parentStack;
    }

    /**
     * @param OutlineInterface $parent
     */
    public function appendParent(OutlineInterface $parent)
    {
        if ( ! $this->hasParent()) {
            $this->setParent($parent);
            return;
        }
        $this->getParentStack()->top()->setParent($parent);
    }

    /**
     * @inheritDoc
     */
    public function getParent(): OutlineInterface
    {
        return $this->parent;
    }

    /**
     * @inheritDoc
     */
    public function hasParent(): bool
    {
        return ! is_null($this->parent);
    }

    /**
     * @param array $a
     * @param array $b
     * @return array
     */
    protected static function mergeArrays(array &$a,array $b)
    {
        foreach ($b as $key => $value) {
            if ( ! isset($a[$key])) {
                $a[$key] = $value;
                continue;
            }
            else {
                if (is_array($value)) {
                    self::mergeArrays($a[$key],$value);
                }
                else {
                    $a[$key] = $value;
                }
            }
        }
        return $a;
    }

    /**
     * @param OutlineInterface $a
     * @param OutlineInterface $b
     * @return OutlineInterface
     */
    public static function override(OutlineInterface $a, OutlineInterface $b):OutlineInterface
    {
        $arrayA = $a->toArray();
        $arrayB = $b->toArray();
        $mergedArray = self::mergeArrays($arrayA,$arrayB);
        $mergedOutline = new static();
        $mergedOutline->configureFromArray($mergedArray);
        if ($a instanceof SampleOutlineProvider && $b instanceof SampleOutlineProvider) {
            $mergedSampleOutline = SampleOutline::override($a->getSampleOutline(),$b->getSampleOutline());
            $mergedOutline->setSampleOutline($mergedSampleOutline);
        }
        if ($a instanceof ContextOutlineProvider && $b instanceof ContextOutlineProvider) {
            $mergedContextOutline = ContextOutline::override($a->getContextOutline(),$b->getContextOutline());
            $mergedOutline->setContextOutline($mergedContextOutline);
        }
        return $mergedOutline;
    }

    /**
     * @inheritDoc
     */
    public function getMergedOutline(): OutlineInterface
    {
        $stack = $this->getParentStack();
        $stack->unshift($this);
        $outline = $stack->shift();
        while ( ! $stack->isEmpty()) {
            $next = $stack->top();
            $stack[$stack->key()] = static::override($outline,$next);
            $outline = $stack->shift();
        }
        return $outline;
    }

    /**
     * @inheritDoc
     */
    public function configureFromArray(array $data)
    {
        if (isset($data['parent'])) {
            $parent = new static();
            $parent->configureFromArray($data['parent']);
            $this->setParent($parent);
        }
    }

}