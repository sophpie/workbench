<?php
/**
 * Created by PhpStorm.
 * User: sophie.beaupuis
 * Date: 05/01/2017
 * Time: 08:21
 */

namespace Sophpie\Workbench\Test\Outline;


use Sophpie\Workbench\Test\Sample\SampleOutline;

/**
 * Interface SampleOutlineProvider
 * @package Sophpie\Workbench\Test\Outline
 */
interface SampleOutlineProvider
{
    /**
     * @return SampleOutline
     */
    public function getSampleOutline():SampleOutline;

    /**
     * @param SampleOutline $sampleOutline
     * @return mixed
     */
    public function setSampleOutline(SampleOutline $sampleOutline);
}