<?php
declare(strict_types = 1);
/**
 * Created by PhpStorm.
 * User: sophie.beaupuis
 * Date: 29/12/2016
 * Time: 16:05
 */

namespace Sophpie\Workbench\Test\TestSuite;


use Sophpie\Workbench\Test\Outline\ContextOutlineProviderTrait;
use Sophpie\Workbench\Test\Outline\OutlineAbstract;
use Sophpie\Workbench\Project\Project;
use Sophpie\Workbench\Test\Context\ContextOutline;
use Sophpie\Workbench\Test\Outline\ProjectAwareTrait;
use Sophpie\Workbench\Test\Outline\SampleOutlineProviderTrait;
use Sophpie\Workbench\Test\Sample\SampleOutline;
use Sophpie\Workbench\Test\TestOutline;
use Sophpie\Workbench\Test\TestOutlineInterface;

/**
 * Class TestSuite
 * @package Sophpie\Workbench\Test\TestSuite
 */
class TestSuite extends OutlineAbstract implements TestOutlineInterface
{
    use SampleOutlineProviderTrait;
    use ContextOutlineProviderTrait;
    use ProjectAwareTrait;
    /**
     * @var array
     */
    protected $testOutlines = [];

    /**
     * @return array
     */
    public function getTestOutlines(): array
    {
        return $this->testOutlines;
    }
    /**
     * @param TestOutline $testOutline
     */
    public function addTestOutline(TestOutline $testOutline)
    {
        $testOutline->setTestSuite($this);
        if ($this->hasProject()){
            $testOutline->setProject($this->project);
        }
        $this->testOutlines[] = $testOutline;
    }

    /**
     * @inheritDoc
     */
    public function configureFromArray(array $data)
    {
        if (isset($data['project'])) {
            $project = new Project();
            $project->configureFromArray($data['project']);
            $this->setProject($project);
        }
        if (isset($data['context'])) {
            $this->contextOutline = new ContextOutline();
            $this->contextOutline->configureFromArray($data['context']);
        }
        if (isset($data['sample'])) {
            $this->sampleOutline = new SampleOutline();
            $this->sampleOutline->configureFromArray($data['sample']);
        }
        foreach ($data['tests'] as $testData)
        {
            $testOutline = new TestOutline();
            $testOutline->configureFromArray($testData);
            $testOutline->setParent($this);
            $this->addTestOutline($testOutline);
        }
    }

    /**
     * @inheritDoc
     */
    public function toArray(): array
    {
        $array = parent::toArray();
        foreach ($this->getTestOutlines() as $testOutline) {
            $array['tests'][] = $testOutline->toArray();
        }
        return $array;
    }


}