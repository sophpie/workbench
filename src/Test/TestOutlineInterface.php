<?php
/**
 * Created by PhpStorm.
 * User: sophie.beaupuis
 * Date: 05/01/2017
 * Time: 09:16
 */

namespace Sophpie\Workbench\Test;


use Sophpie\Workbench\Test\Outline\ContextOutlineProvider;
use Sophpie\Workbench\Test\Outline\OutlineInterface;
use Sophpie\Workbench\Test\Outline\ProjectAwareInterface;
use Sophpie\Workbench\Test\Outline\SampleOutlineProvider;

/**
 * Interface TestOutlineInterface
 * @package Sophpie\Workbench\Test
 */
interface TestOutlineInterface extends OutlineInterface,
    SampleOutlineProvider,ContextOutlineProvider,ProjectAwareInterface
{

}