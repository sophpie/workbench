<?php
declare(strict_types = 1);
/**
 * Created by PhpStorm.
 * User: sophie.beaupuis
 * Date: 29/12/2016
 * Time: 16:47
 */

namespace Sophpie\Workbench\Test\Result;


class ResultCollection implements \JsonSerializable
{
    /**
     * @var array
     */
    protected $results = [];

    /**
     * @return array
     */
    public function getResults(): array
    {
        return $this->results;
    }

    /**
     * @param Result $result
     */
    public function addResult(Result $result)
    {
        $this->results[] = $result;
    }

    /**
     * @inheritDoc
     */
    public function jsonSerialize()
    {
        $jsonArray = [];
        foreach ($this->getResults() as $result) {
            $jsonArray[] = $result->jsonSerialize();
        }
        return $jsonArray;
    }


}