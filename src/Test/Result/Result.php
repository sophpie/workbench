<?php
declare(strict_types = 1);
/**
 * Created by PhpStorm.
 * User: sophie.beaupuis
 * Date: 22/12/2016
 * Time: 10:06
 */

namespace Sophpie\Workbench\Test\Result;

use Sophpie\Workbench\Test\Test;
use Sophpie\Workbench\Test\Outline\OutlineAbstract;


/**
 * Class Result
 * @package Sophpie\Workbench\Test\Result
 */
class Result extends OutlineAbstract
{
    /**
     * @var mixed
     */
    protected $executionResult;

    /**
     * @var bool
     */
    protected $isSuccess = true;

    /**
     * @var array
     */
    protected $probeResults = [];

    /**
     * @var array
     */
    protected $assertionResults = [];

    /**
     * @var Test;
     */
    protected $test;

    /**
     * @return mixed
     */
    public function getExecutionResult()
    {
        return $this->executionResult;
    }

    /**
     * @param mixed $executionResult
     */
    public function setExecutionResult($executionResult)
    {
        $this->executionResult = $executionResult;
    }

    /**
     * @return bool
     */
    public function isSuccess(): bool
    {
        return $this->isSuccess;
    }

    /**
     * @param bool $isSuccess
     */
    public function setSuccess(bool $isSuccess)
    {
        $this->isSuccess = $isSuccess;
    }


    /**
     * @return array
     */
    public function getProbeResults(): array
    {
        return $this->probeResults;
    }

    /**
     * @param string $probeName
     * @param $probeResult
     */
    public function addProbeResult(string $probeName,array $probeResult)
    {
        $this->probeResults[$probeName] = $probeResult;
    }

    /**
     * @return array
     */
    public function getAssertionResults(): array
    {
        return $this->assertionResults;
    }

    /**
     * @return Test
     */
    public function getTest()
    {
        return $this->test;
    }

    /**
     * @param Test $test
     */
    public function setTest(Test $test)
    {
        $this->test = $test;
    }

    /**
     * @param string $assertionName
     * @param string $dataPath
     * @param $value
     * @param bool $isSuccess
     */
    public function addAssertionResult(string $assertionName,string $dataPath,$value,bool $isSuccess)
    {
        if ( ! isset($this->assertionResults[$assertionName])) {
            $this->assertionResults[$assertionName] = [];
        }
        $this->assertionResults[$assertionName][] = [
            'dataPath' => $dataPath,
            'value' => $value,
            'isSuccess' =>$isSuccess
        ];
    }

    /**
     * @inheritDoc
     */
    public function toArray():array
    {
        $jsonArray = [
            'isSuccess' => $this->isSuccess(),
            'executionResult' => $this->getExecutionResult(),
        ];
        $jsonArray['probes'] = [];
        foreach ($this->getProbeResults() as $probeName => $probeResult) {
            $jsonArray['probes'][$probeName] = $probeResult;
        }
        $jsonArray['assertions'] = [];
        foreach ($this->getAssertionResults() as $assertionName => $assertionResults) {
            foreach ($assertionResults as $assertionResult) {
                if ( ! isset($jsonArray['assertions'][$assertionName])) {
                    $jsonArray['assertions'][$assertionName] = [];
                }
                $jsonArray['assertions'][$assertionName][] = $assertionResult;
            }
        }
        if ($this->getTest()) {
            $jsonArray['test'] = $this->getTest()->jsonSerialize();
        }
        return $jsonArray;
    }

    /**
     * @param array $data
     */
    public function configureFromArray(array $data)
    {
        $this->setSuccess((bool)$data['isSuccess']);
        $this->setExecutionResult($data['executionResult']);
        foreach ($data['probes'] as $probeName => $probeData) {
            $this->addProbeResult($probeName,$probeData);
        }
        foreach ($data['assertions'] as $assertionName => $assertionData) {
            foreach ($assertionData as $result) {
                $this->addAssertionResult($assertionName,$result['dataPath'],$result['value'],$result['isSuccess']);
            }
        }
    }
}