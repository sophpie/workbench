<?php
declare(strict_types = 1);
/**
 * Created by PhpStorm.
 * User: sophie.beaupuis
 * Date: 28/12/2016
 * Time: 09:37
 */

namespace Sophpie\Workbench\Test;


use Sophpie\Workbench\Test\Outline\OutlineInterface;
use Sophpie\Workbench\Test\Context\ContextOutline;
use Sophpie\Workbench\Test\Sample\SampleOutline;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class TestBuilder
 * Collect Sample and Context outline and merge it.
 * Then use Sample and Context builder to build actual test object
 *
 * @package Sophpie\Workbench\Test
 */
class TestBuilder
{
    use ContainerAwareTrait;

    /**
     * TestBuilder constructor.
     */
    public function __construct(ContainerInterface $container)
    {
        $this->setContainer($container);
    }

    /**
     * @param TestOutline $testOutline
     * @return Test
     */
    public function buildTest(TestOutline $testOutline):Test
    {
        $test = new Test();
        $testOutline->getContextOutline();
        $this->container->get('sampleBuilder')->build($testOutline->getSampleOutline());
        $test->setSample($this->container->get('sample'));
        $this->container->get('contextBuilder')->build($testOutline->getContextOutline());
        $test->setContext($this->container->get('context'));
        $this->container->compile();
        return $test;
    }
}