<?php
declare(strict_types = 1);
/**
 * Created by PhpStorm.
 * User: sophie.beaupuis
 * Date: 28/12/2016
 * Time: 09:28
 */

namespace Sophpie\Workbench\Test;


use Sophpie\Workbench\Test\Outline\ContextOutlineProviderTrait;
use Sophpie\Workbench\Test\Outline\OutlineAbstract;
use Sophpie\Workbench\Project\Project;
use Sophpie\Workbench\Test\Context\ContextOutline;
use Sophpie\Workbench\Test\Outline\ProjectAwareTrait;
use Sophpie\Workbench\Test\Outline\SampleOutlineProviderTrait;
use Sophpie\Workbench\Test\Sample\SampleOutline;

/**
 * Class TestOutline
 * @package Sophpie\Workbench\Test
 */
class TestOutline extends OutlineAbstract implements TestOutlineInterface
{
    use ProjectAwareTrait;
    use SampleOutlineProviderTrait;
    use ContextOutlineProviderTrait;

    /**
     * @var array
     */
    protected $testCollection = [];

    /**
     * TestOutline constructor.
     */
    public function __construct()
    {
        $this->sampleOutline = new SampleOutline();
        $this->contextOutline = new ContextOutline();
    }


    /**
     * @param array $data
     */
    public function configureFromArray(array $data)
    {
        if (isset($data['tests'])) {
            foreach ($data['tests'] as $testData) {
                $testOutline = new self();
                $testOutline->configureFromArray($testData);
                $testOutline->appendParent($this);
                $this->testCollection[] = $testOutline;
            }
        }
        if (isset($data['sample'])) {
            $this->sampleOutline = new SampleOutline();
            $this->sampleOutline->configureFromArray($data['sample']);
        }
        if (isset($data['context'])) {
            $this->contextOutline = new ContextOutline();
            $this->contextOutline->configureFromArray($data['context']);
        }
        if (isset($data['project'])) {
            $this->project = new Project();
            $this->project->configureFromArray($data['project']);
        }
    }

    /**
     * @inheritDoc
     */
    public function toArray():array
    {
        $jsonArray = [];
        if ($this->hasProject()) {
            $jsonArray['project'] = $this->getProject()->toArray();
        }
        $jsonArray['sample'] = $this->getSampleOutline()->toArray();
        $jsonArray['context'] = $this->getContextOutline()->toArray();
        if (count($this->testCollection) > 0) {
            $jsonArray['tests'] = [];
            foreach ($this->getTestCollection() as $test) {
                $jsonArray['tests'][] = $test->toArray();
            }
        }
        return $jsonArray;
    }

    /**
     * @return bool
     */
    public function isSuite():bool
    {
        return count($this->testCollection) > 0;
    }

    /**
     * @return array
     */
    public function getTestCollection(): array
    {
        return $this->testCollection;
    }

    /**
     * @return array
     */
    public function getMergedOutlineChildren():array
    {
        $parentData = $this->toArray();
        unset($parentData['tests']);
        $childTests = [];
        foreach ($this->getTestCollection() as $test) {
            $childTestData = self::mergeArrays($parentData,$test->toArray());
            $childTestOutline = new self();
            $childTestOutline->configureFromArray($childTestData);
            $childTests[] = $childTestOutline;
        }
        return $childTests;
    }
}