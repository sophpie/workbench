<?php
declare(strict_types = 1);
/**
 * Created by PhpStorm.
 * User: sophie.beaupuis
 * Date: 06/01/2017
 * Time: 11:30
 */

namespace Sophpie\Workbench\Console;


use Sophpie\Workbench\Workbench;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\Definition;

/**
 * Class WorkbenchApplication
 * @package Sophpie\Workbench\Console
 */
class WorkbenchApplication extends Application
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @var string
     */
    protected $applicationConfigPath;

    /**
     * @inheritDoc
     */
    public function __construct(string $applicationConfigPath)
    {
        parent::__construct('workbench', '');
        $this->applicationConfigPath = $applicationConfigPath;
        $this->buildContainer();
    }

    /**
     *
     */
    protected function buildContainer()
    {
        $this->container = new ContainerBuilder();
        $this->container->setDefinition(
            'workbench',
            new Definition(Workbench::class,[$this->applicationConfigPath])
        );

    }

    /**
     * @inheritDoc
     */
    public function add(Command $command)
    {
        if ($command instanceof ContainerAwareInterface) {
            $command->setContainer($this->getContainer());
        }
        return parent::add($command);
    }


    /**
     * @return ContainerInterface
     */
    protected function getContainer(): ContainerInterface
    {
        return $this->container;
    }

    /**
     * @param ContainerInterface $container
     */
    protected function setContainer(ContainerInterface $container)
    {
        $this->container = $container;
    }




}