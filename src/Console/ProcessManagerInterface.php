<?php
declare(strict_types = 1);
/**
 * Created by PhpStorm.
 * User: sophie.beaupuis
 * Date: 26/01/2017
 * Time: 15:40
 */
namespace Sophpie\Workbench\Console;

use Sophpie\Workbench\Test\Result\Result;


/**
 * Class SqLiteProcessManager
 * @package Sophpie\Workbench\Console
 */
interface ProcessManagerInterface
{
    /**
     * @param Result $result
     */
    public function saveResult(Result $result, string $reportId);

    /**
     * @param string $reportId
     * @param int $pid
     */
    public function startProcess(string $reportId, int $pid);

    /**
     * @param string $reportId
     * @param int $pid
     */
    public function endProcess(string $reportId, int $pid);

    /**
     * @param string $reportId
     * @return bool
     */
    public function isReportFinished(string $reportId): bool;

    /**
     * @param string $reportId
     * @return array
     */
    public function getResults(string $reportId):array;
}