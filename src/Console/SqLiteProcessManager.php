<?php
declare(strict_types = 1);
/**
 * Created by PhpStorm.
 * User: sophie.beaupuis
 * Date: 26/01/2017
 * Time: 15:29
 */

namespace Sophpie\Workbench\Console;


use Sophpie\Workbench\Test\Result\Result;

/**
 * Class SqLiteProcessManager
 * @package Sophpie\Workbench\Console
 */
class SqLiteProcessManager implements ProcessManagerInterface
{
    /**
     * @var \SQLite3
     */
    protected $sqLiteManager;

    /**
     * SqLiteProcessManager constructor.
     */
    public function __construct()
    {
        $this->sqLiteManager = new \SQLite3('reports.db');
        $this->sqLiteManager->exec("CREATE TABLE IF NOT EXISTS results (id TEXT,result TEXT);");
        $this->sqLiteManager->exec("CREATE TABLE IF NOT EXISTS processes (id INT,pid INT);");
    }


    /**
     * @param Result $result
     */
    public function saveResult(Result $result,string $reportId)
    {
        $statement = $this->sqLiteManager->prepare('INSERT INTO results (id,result) VALUES (:id,:data);');
        $statement->bindValue(':id',$reportId);
        $statement->bindValue(':data',json_encode($result->toArray(),JSON_FORCE_OBJECT));
        $statement->execute();
    }

    /**
     * @param string $reportId
     * @param int $pid
     */
    public function startProcess(string $reportId,int $pid)
    {
        $statement = $this->sqLiteManager->prepare('INSERT INTO processes (id,pid) VALUES (:id,:pid);');
        $statement->bindValue(':id',$reportId);
        $statement->bindValue(':pid',$pid);
        $statement->execute();
    }

    /**
     * @param string $reportId
     * @param int $pid
     */
    public function endProcess(string $reportId,int $pid)
    {
        $statement = $this->sqLiteManager->prepare('DELETE INTO processes WHERE id=:id AND pid=:pid);');
        $statement->bindValue(':id',$reportId);
        $statement->bindValue(':pid',$pid);
        $statement->execute();
    }

    /**
     * @param string $reportId
     * @return bool
     */
    public function isReportFinished(string $reportId):bool
    {
        $statement = $this->sqLiteManager->prepare('SELECT pid FROM processes WHERE id=:id;');
        $result = $statement->execute();
        if ( ! $result->numColumns() || $result->columnType(0) == SQLITE3_NULL) return true;
        return false;
    }

    /**
     * @inheritDoc
     */
    public function getResults(string $reportId): array
    {
        $statement = $this->sqLiteManager->prepare('SELECT * FROM results WHERE id=:id;');
        $statement->bindValue(':id',$reportId);
        $SqLiteResults = $statement->execute();
        $results = [];
        while($item = $SqLiteResults->fetchArray(SQLITE3_ASSOC)) {
            $resultJson = $item['result'];
            $result = new Result();
            $result->configureFromArray(json_decode($resultJson,true));
            $results[] = $result;
        }
        return $results;
    }


}