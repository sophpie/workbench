<?php
declare(strict_types = 1);
/**
 * Created by PhpStorm.
 * User: sophie.beaupuis
 * Date: 06/01/2017
 * Time: 11:11
 */

namespace Sophpie\Workbench\Console\Command;


use Sophpie\Workbench\Console\ProcessManagerInterface;
use Sophpie\Workbench\Console\SqLiteProcessManager;
use Sophpie\Workbench\Report\Report;
use Sophpie\Workbench\Test\Result\Result;
use Sophpie\Workbench\Test\TestOutline;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\Process\Process;

/**
 * Class RunTestCommand
 * @package Sophpie\Workbench\Console
 */
class RunTestCommand extends WorkbenchCommand
{
    const OPTION_DIRECTORY = 'directory';

    use ContainerAwareTrait;

    /**
     * @var string
     */
    protected $reportId;

    /**
     * @var ProcessManagerInterface
     */
    protected $processManager;

    /**
     * @inheritDoc
     */
    protected function configure()
    {
        $this->setName("wb:run");
        $this->addOption('directory','d',InputOption::VALUE_OPTIONAL,'In which directory to fetch for test definition');
        $this->addOption('testFile','f',InputOption::VALUE_OPTIONAL,'Which test file');
        $this->addOption('json','j',InputOption::VALUE_OPTIONAL,'JSON data');
        $this->addOption('report-file','r',InputOption::VALUE_OPTIONAL,'Report file path & name','report.json');
        $this->addOption('id','i',InputOption::VALUE_OPTIONAL,'Report ID');
    }

    /**
     * @inheritDoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->processManager = new SqLiteProcessManager();
        $this->prepareResultStack($input);
        if ( ! empty($input->getOption('testFile'))) {
            $filePath = $input->getOption('testFile');
            $this->executeFile($filePath,$input,$output);
        }
        elseif ( ! empty($input->getOption('directory'))) {
            $this->executeFiles($input,$output);
        }
        if ( ! empty($input->getOption('json'))) {
            $jsonString = $input->getOption('json');
            $jsonData = json_decode($jsonString,true);
            $testOutline = new TestOutline();
            $testOutline->configureFromArray($jsonData);
            $this->executeTestOutline($testOutline,$input,$output);
        }
        if (empty($input->getOption('id'))) {
            if ($this->processManager->isReportFinished($this->reportId)) {
                $results = $this->processManager->getResults($this->reportId);
                var_dump($results);
            }
        }
    }

    /**
     * @param TestOutline $testOutline
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function executeTestOutline(TestOutline $testOutline,InputInterface $input, OutputInterface $output)
    {
        if ($testOutline->isSuite()) {
            foreach ($testOutline->getMergedOutlineChildren() as $test) {
                $jsonData = json_encode($test->toArray(),JSON_FORCE_OBJECT);
                $jsonData = addslashes($jsonData);
                $this->sendToNewProcess([
                    'json' => '"' . $jsonData . '"',
                    'id' => $this->reportId
                ]);
            }
        }
        else {
            $workbench = $this->getWorkbench();
            $result = $workbench->runSingleTest($testOutline);
            $this->processManager->saveResult($result,$this->reportId);
        }

    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function executeFiles(InputInterface $input,OutputInterface $output)
    {
        if ($input->hasOption(self::OPTION_DIRECTORY) && $input->getOption('directory')) {
            $directory = $input->getOption(self::OPTION_DIRECTORY);
            foreach (scandir($directory) as $item) {
                $file = $directory . DIRECTORY_SEPARATOR . $item;
                if ( ! is_file($file)) continue;
                $pathInfo = pathinfo($file);
                if ($pathInfo['extension'] == 'php') {
                    $this->sendToNewProcess([
                        'testFile' => $file,
                        'id' => $this->reportId
                    ]);
                }
            }
        }
    }

    /**
     * @param $filePath
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function executeFile($filePath,InputInterface $input, OutputInterface $output)
    {
        $testOutline = new TestOutline();
        $testOutline->configureFromArray(include $filePath);
        $this->executeTestOutline($testOutline,$input,$output);
    }

    /**
     * @param InputInterface $input
     */
    protected function prepareResultStack(InputInterface $input)
    {
        if ( ! empty($input->getOption('id'))) {
            $this->reportId = $input->getOption('id');
        }
        else {
            $this->reportId = md5((string)time());
        }
    }

    /**
     * @param array $options
     * @return Process
     */
    protected function sendToNewProcess(array $options = [])
    {
        $commandLine = './bin/console wb:run';
        foreach ($this->getDefinition()->getOptions() as $commandOption) {
            $optionName = $commandOption->getName();
            $optionShortName = $commandOption->getShortcut();
            if (isset($options[$optionName])) {
                $commandLine .= ' -' . $optionShortName . ' ' . $options[$optionName];
            }
        }
        $commandLine .= ' -v';
        $process = new Process($commandLine);
        $process->start();
        $this->processManager->startProcess($this->reportId,$process->getPid());
        foreach ($process as $type => $data) {
            if ($type == Process::ERR) {
                var_dump($data);
                continue;
            }
        }
        $process->stop();
    }

}