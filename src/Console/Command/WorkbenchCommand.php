<?php
declare(strict_types = 1);
/**
 * Created by PhpStorm.
 * User: sophie.beaupuis
 * Date: 06/01/2017
 * Time: 11:34
 */

namespace Sophpie\Workbench\Console\Command;


use Sophpie\Workbench\Workbench;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class WorkbenchCommand
 * @package Sophpie\Workbench\Console\Command
 */
class WorkbenchCommand extends Command implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * @return ContainerInterface
     */
    public function getContainer():ContainerInterface
    {
        return $this->container;
    }

    /**
     * @return Workbench
     */
    protected function getWorkbench():Workbench
    {
        return $this->getContainer()->get('workbench');
    }

}