<?php
declare(strict_types = 1);
/**
 * Created by PhpStorm.
 * User: sophie.beaupuis
 * Date: 06/01/2017
 * Time: 11:49
 */
return [
    'sample' => [
        'functionName' => 'add',
    ],
    'context' => [
        'instance' => [
            'type' => 'definition',
            'data' => [
                'class' => \Sophpie\Dummy\Calculator::class
            ],
        ],
        'arguments' => [3,6],
        'assertionTests' => [
            [
                'assertion' => [
                    'type' => 'definition',
                    'data' => [
                        'class' => \Sophpie\Workbench\Assertion\AssertEquality::class
                    ],
                ],
                'dataPath' => 'result',
                'expectedValue' => 9
            ]
        ],
    ],
];