<?php
declare(strict_types = 1);
/**
 * Created by PhpStorm.
 * User: sophie.beaupuis
 * Date: 06/01/2017
 * Time: 15:45
 */
return [
    'sample' => [
        'functionName' => 'add',
    ],
    'context' => [
        'instance' => [
            'type' => 'definition',
            'data' => [
                'class' => \Sophpie\Dummy\Calculator::class
            ],
        ],
    ],
    'tests' => [
        //1 + 2 = 3
        [
            'context' => [
                'arguments' => [1,2],
                'assertionTests' => [
                    [
                        'assertion' => [
                            'type' => 'definition',
                            'data' => [
                                'class' => \Sophpie\Workbench\Assertion\AssertEquality::class
                            ],
                        ],
                        'dataPath' => 'result',
                        'expectedValue' => 3
                    ]
                ],
            ],

        ],
        //1 + 2 = 4
        [
            'context' => [
                'arguments' => [1,2],
                'assertionTests' => [
                    [
                        'assertion' => [
                            'type' => 'definition',
                            'data' => [
                                'class' => \Sophpie\Workbench\Assertion\AssertEquality::class
                            ],
                        ],
                        'dataPath' => 'result',
                        'expectedValue' => 4
                    ]
                ],
            ],

        ],

    ],
];