<?php
declare(strict_types = 1);
/**
 * Created by PhpStorm.
 * User: sophie.beaupuis
 * Date: 15/01/2017
 * Time: 14:40
 */
return  [
    'sample' => [
        'functionName' => 'isValid',
    ],
    'context' => [
        'instance' => [
            'type' => 'definition',
            'data' => [
                'class' => \Sophpie\Workbench\Assertion\AssertEquality::class,
            ]
        ],
        'arguments' => [
            'truc','truc'
        ],
        'assertionTests' => [
            [
                'assertion' => [
                    'type' => 'definition',
                    'data' => [
                        'class' => \Sophpie\Workbench\Assertion\AssertEquality::class
                    ],
                ],
                'dataPath' => 'result',
                'expectedValue' => true
            ]
        ],
    ],
];