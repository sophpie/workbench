<?php
declare(strict_types = 1);
/**
 * Created by PhpStorm.
 * User: sophie.beaupuis
 * Date: 15/01/2017
 * Time: 15:13
 */
return [
    'sample' => [
        'functionName' => 'parseAsDefinition',
    ],
    'context' => [
        'instance' => [
            'type' => 'definition',
            'data' => [
                'class' => \Sophpie\Workbench\DependencyInjection\ContainerNode::class,
                'arguments' => [
                    [],
                    [
                        'type' => 'reference',
                        'data' => [
                            'serviceId' => 'service_container'
                        ],
                    ]
                ],
            ],
        ],
        'arguments' => [
        ],
        'assertionTests' => [
            [
                'assertion' => [

                ],
                'dataPath' => '',
                'expectedValue' => '',
            ],
        ],
    ],
];