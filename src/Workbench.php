<?php
declare(strict_types = 1);
/**
 * Created by PhpStorm.
 * User: sophie.beaupuis
 * Date: 19/12/2016
 * Time: 08:39
 */

namespace Sophpie\Workbench;


use Sophpie\Workbench\DependencyInjection\Extension\PluginExtension;
use Sophpie\Workbench\Project\Scan\ClassScanner;
use Sophpie\Workbench\Test\Context\ContextBuilder;
use Sophpie\Workbench\Test\DataMiner\DataMiner;
use Sophpie\Workbench\Test\Result\Result;
use Sophpie\Workbench\Test\Sample;
use Sophpie\Workbench\Test\TestBuilder;
use Sophpie\Workbench\Test\TestManager;
use Sophpie\Workbench\Test\TestOutline;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\Config\Loader\DelegatingLoader;
use Symfony\Component\Config\Loader\LoaderResolver;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Loader\ClosureLoader;
use Symfony\Component\DependencyInjection\Loader\DirectoryLoader;
use Symfony\Component\DependencyInjection\Loader\IniFileLoader;
use Symfony\Component\DependencyInjection\Loader\PhpFileLoader;
use Symfony\Component\DependencyInjection\Loader\XmlFileLoader;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\DependencyInjection\Reference;
use Symfony\Component\EventDispatcher\ContainerAwareEventDispatcher;
use Symfony\Component\EventDispatcher\DependencyInjection\RegisterListenersPass;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;


/**
 * Class Workbench
 * Testing sub-application wrapper.
 * @package Sophpie\Workbench
 */
class Workbench
{
    const EVENT_INIT = 'workbench.init';
    const EVENT_EXTENSIONS_LOADING = "workbench.extensions.loading";

    /**
     * @var ContainerBuilder
     */
    protected $container;

    /**
     * @var EventDispatcherInterface
     */
    protected $dispatcher;

    /**
     * @var bool
     */
    protected $isInitiated = false;

    /**
     * @var string
     */
    protected $configPath;

    /**
     * Workbench constructor.
     * @param string $workbenchConfigPath
     */
    public function __construct(string $workbenchConfigPath)
    {
        $this->container = new  ContainerBuilder();
        $this->dispatcher = new EventDispatcher();
        $this->container->registerExtension(new PluginExtension());
        $this->getConfigLoader()->load($workbenchConfigPath);
        $this->container->addCompilerPass(
            new RegisterListenersPass('dispatcher','workbench.listener','workbench.subscriber')
        );
        $this->container->addCompilerPass(
            new RegisterListenersPass('workbenchDispatcher','truc','workbench.plugin')
        );
        $this->container->addDefinitions(
            [
                'testManager' => new Definition(TestManager::class, [new Reference('dispatcher')]),
                'sampleBuilder' => new Definition(Sample\SampleBuilder::class,[new Reference('service_container')]),
                'contextBuilder' => new Definition(ContextBuilder::class,[new Reference('service_container')]),
                'testBuilder' => new Definition(TestBuilder::class,[new Reference('service_container')]),
                'classScanner' => new Definition(ClassScanner::class),
                'dataMiner' => new Definition(DataMiner::class),
                'dispatcher' => new Definition(ContainerAwareEventDispatcher::class,[new Reference('service_container')]),
                'workbenchDispatcher'=> new Definition(ContainerAwareEventDispatcher::class,[new Reference('service_container')]),
            ]
        );
        $this->container->set('workbench',$this);
        $this->container->getCompiler()->compile($this->container);
        $workbenchEvent = new WorkbenchEvent();
        $workbenchEvent->setContainerBuilder($this->container);
        $workbenchDispatcher = $this->container->get('workbenchDispatcher');
        $workbenchDispatcher->dispatch(self::EVENT_INIT,$workbenchEvent);
        $workbenchDispatcher->dispatch(self::EVENT_EXTENSIONS_LOADING,$workbenchEvent);
        //@todo manage container caching
    }

    /**
     * @return FileLocator
     */
    protected function getConfigLoader():DelegatingLoader
    {
        $locator = new FileLocator([$this->configPath]);
        $resolver = new LoaderResolver([
            new XmlFileLoader($this->container, $locator),
            new YamlFileLoader($this->container, $locator),
            new IniFileLoader($this->container, $locator),
            new PhpFileLoader($this->container, $locator),
            new DirectoryLoader($this->container, $locator),
            new ClosureLoader($this->container),
        ]);
        return new DelegatingLoader($resolver);
    }

    /**
     * @param array $testData
     * @return Result
     */
    public function run(array $testData):Result
    {
        $testOutline = new TestOutline();
        $testOutline->configureFromArray($testData);
        if ($testOutline->isSuite()) {
            return $this->runSuite($testOutline);
        }
        else {
            return $this->runSingleTest($testOutline);
        }

    }

    /**
     * @param TestOutline $testOutline
     * @return Result
     */
    public function runSuite(TestOutline $testOutline):Result
    {
        foreach ($testOutline->getTestCollection() as $test) {
            $this->runSingleTest($test->getMergedOutline());
        }
        return new Result();
    }

    /**
     * @param TestOutline $testOutline
     * @return mixed
     */
    public function runSingleTest(TestOutline $testOutline):Result
    {
        $test = $this->container->get('testBuilder')->buildTest($testOutline);
        $result = $this->container->get('testManager')->runTest($test);
        return $result;
    }

    /**
     * @return ContainerBuilder
     */
    public function getContainer(): ContainerBuilder
    {
        return $this->container;
    }
}