Workbench
===
Workbench if an unit testing application for PHP. The purpose of this is to allow quick and effort less unit testing 
of your code without writing any PHP code.


Main concepts
---
**no PHP code**
Coding without coding allows a quick and industrialized test generation. The main goal of Workbech is to reduce as much 
as possible the time spent on testing. Today testing can cost 30% of project whole time. Workbenc is designed to reduce 
this ratio to a minimu. 

**"Sample" and "Context"**
Workbench main principle is that a unit test consist in executing a function and check if the outcome of this function
call is the one expected.
The outcome of a function is not only its returned value, but also any exception thrown and modifications of other 
objects outside function itself. 
To put in words, a unit test in Workbench is composed of two main parts : 
* What we call "Sample" which is the callable we are testing
* What we call  "Context" which stand for all what has influence or be influenced by function execution : function 
parameters, session data, class instance properties, etc.

**Persistence**
Non of Unit testing framework allow you to persist test results. Workbench running with a database which allows it to 
persists tests and results for further analysis.

**Probes**
Init testing is a thing but there are a lot of things we can do while testing. By example it could be useful to analyse
performance, proceed with statical analysis,etc.
For that Workbench and its signal oriented design allows you to add Probes to the tests. These probes will collect 
values during test and add data to test result.