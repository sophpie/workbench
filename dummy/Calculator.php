<?php
declare(strict_types = 1);

/**
 * Created by PhpStorm.
 * User: sophie.beaupuis
 * Date: 21/12/2016
 * Time: 13:56
 */

namespace Sophpie\Dummy;

/**
 * Class Calculator
 * @package Sophpie\Dummy
 */
class Calculator
{
    /**
     * @param float $a
     * @param float $b
     * @return float
     */
    public function add(float $a,float $b):float
    {
        return $a + $b;
    }
}