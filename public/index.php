<?php
declare(strict_types = 1);
/**
 * Created by PhpStorm.
 * User: sophie.beaupuis
 * Date: 19/12/2016
 * Time: 08:35
 */
use Sophpie\Workbench\Plugin\Api\Gateway;
$loader = require "../vendor/autoload.php";
\Symfony\Component\Debug\Debug::enable();
$request = \Symfony\Component\HttpFoundation\Request::createFromGlobals();

$gateway  = new Gateway('dev',true,__DIR__ . '/../Config/application.config.yml');
$response = $gateway->handle($request);
$response->send();



